﻿Shader "Custom/MapCellShader"
{
	Properties
	{
		[PerRendererData] _MainTex("Sprite Texture", 2D) = "white" {}
		[PerRendererData] _Select("Select", Float) = 0
		[PerRendererData] _T0("Time", Float) = 0
		_Color("Tint", Color) = (1,1,1,1)
	}

		SubShader
		{
			Tags
			{
				"Queue" = "Transparent"
				"IgnoreProjector" = "True"
				"RenderType" = "Transparent"
				"PreviewType" = "Plane"
				"CanUseSpriteAtlas" = "True"
			}

			Cull Off
			Lighting Off
			ZWrite Off
			Blend One OneMinusSrcAlpha

			Pass
			{
			CGPROGRAM
				#pragma vertex vert
				#pragma fragment frag
				#include "UnityCG.cginc"

				struct appdata_t
				{
					float4 vertex   : POSITION;
					float4 color    : COLOR;
					float2 texcoord : TEXCOORD0;
				};

				struct v2f
				{
					float4 vertex   : SV_POSITION;
					float4 color : COLOR;
					float2 texcoord  : TEXCOORD0;
				};

				float4 _Color;
				float _Select;
				float _T0;
				v2f vert(appdata_t IN)
				{
					v2f OUT;
					OUT.vertex = UnityObjectToClipPos(IN.vertex);
					OUT.texcoord = IN.texcoord;
					OUT.color = IN.color * _Color;

					return OUT;
				}

				sampler2D _MainTex;


				float4 frag(v2f IN) : SV_Target
				{
					float4 c = tex2D(_MainTex, IN.texcoord) * IN.color;
					float t = clamp((_Time.y - _T0) * 2, 0, 1);
					float select = _Select - ( 1-t);
					if (_Select == 0)
						select = 0;
					c.rgb *= (1 + select * 0.4);
					c.rgb *= c.a;
					return c;
				}
			ENDCG
			}
		}
}
