﻿Shader "Custom/GrayScaleShaderUI"
{
	Properties
	{
		[PerRendererData] _MainTex("Sprite Texture", 2D) = "white" {}
		_Color("Tint", Color) = (1,1,1,1)

	}

		SubShader
		{
			Tags
			{
				"Queue" = "Transparent"
				"IgnoreProjector" = "True"
				"RenderType" = "Transparent"
				"PreviewType" = "Plane"
				"CanUseSpriteAtlas" = "True"
			}
			Cull Off
			Lighting Off
			ZWrite Off
			ZTest[unity_GUIZTestMode]
			Blend SrcAlpha OneMinusSrcAlpha

			Pass
			{
				Name "Default"
			CGPROGRAM
				#pragma vertex vert
				#pragma fragment frag
				#pragma target 2.0

				#include "UnityCG.cginc"
				#include "UnityUI.cginc"

				struct appdata_t
				{
					float4 vertex   : POSITION;
					float4 color    : COLOR;
					float2 texcoord : TEXCOORD0;
					UNITY_VERTEX_INPUT_INSTANCE_ID
				};

				struct v2f
				{
					float4 vertex   : SV_POSITION;
					float4 color : COLOR;
					float2 texcoord  : TEXCOORD0;
					float4 worldPosition : TEXCOORD1;
					UNITY_VERTEX_OUTPUT_STEREO
				};

				sampler2D _MainTex;
				float4 _Color;
				//float4 _HealthColor, _HitColor, _LoseColor, _StripeColor,_Color, _VoidColor;


				v2f vert(appdata_t v)
				{
					v2f OUT;
					OUT.worldPosition = v.vertex;
					OUT.vertex = UnityObjectToClipPos(OUT.worldPosition);
					OUT.texcoord = v.texcoord;
					OUT.color = v.color * _Color;
					return OUT;
				}
				float4 Grey(float4 color)
				{
					float t = (color.x + color.y + color.z) / 3.0;
					color = float4(t,t,t,color.a);
					return color;
				}

				fixed4 frag(v2f IN) : SV_Target
				{
					float4 color = tex2D(_MainTex, IN.texcoord);// *IN.color;
					
					return Grey(color);
				}

			ENDCG
			}
		}
}
