﻿using UnityEngine;
public interface IMapCellObj
{
    GameObject gameObject { get; }
    IMapCell MapCell { get; set; }

    void SelectEvent();
    void Move(IMapCell mapCell);
    void MoveTo(IMapCellObj mapCellObj);
    void Use(IMapCellObj mapCellObj);
    void OpenInfo();
}
