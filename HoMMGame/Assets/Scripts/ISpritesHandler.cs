﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public interface ISpritesHandler
{
    List<Sprite> Sprites { get; set; }
    Sprite GetRandomSprite();
    Sprite GetSpriteFromId(int id);
}
