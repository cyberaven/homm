﻿using UnityEngine;
using System.Collections;

public class TurnSideHandler : MonoBehaviour
{
    public static TurnSideHandler instance;

    EnumSide _side;
    public EnumSide Side { get => _side; set => _side = value; }

    private void Start()
    {
        instance = this;

        Side = EnumSide.Player;

        //SelectManager.ChangeTurnSideEve += ChangeTurnSide;
    }

    void ChangeTurnSide()
    {
        if(Side == EnumSide.Player)
        {
            Side = EnumSide.Monster;
        }
        else
        {
            Side = EnumSide.Player;
        }
    }
}
