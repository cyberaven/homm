﻿using UnityEngine;
using System.Collections;

public class BotBrain : MonoBehaviour
{
    private void Update()
    {
        if(TurnSideHandler.instance.Side == EnumSide.Monster)
        {
            SelectUnit();
            SelectMapCell();            
        }
    }


    void SelectUnit()
    {
        GameObject[] unitList = GameObject.FindGameObjectsWithTag("Unit");

        foreach(GameObject unitGO in unitList)
        {
            IUnit unit = unitGO.GetComponent<IUnit>();
            if(unit.Side == EnumSide.Monster)
            {
               // unit.SelectEvent();
            }
        }
    }

    void SelectMapCell()
    {
        GameObject[] mapCellList = GameObject.FindGameObjectsWithTag("MapCell");
        IMapCell mapCell = mapCellList[Random.Range(0, mapCellList.Length)].GetComponent<IMapCell>();
        mapCell.SelectEvent();
    }
}
