﻿using System;
using UnityEngine;
using UnityEngine.UI;

public class PauseMenu : Panel
{
    [SerializeField] private Button SaveGameBtn;
    [SerializeField] private Button LoadGameBtn;
    [SerializeField] private Button SettingsBtn;
    [SerializeField] private Button ReloadGameBtn;
    [SerializeField] private Button MainMenuBtn;
    [SerializeField] private Button QuitBtn;
    [SerializeField] private Button BackBtn;

    private void Awake()
    {
        SaveGameBtn.onClick.AddListener(SaveGameBtnClick);
        LoadGameBtn.onClick.AddListener(LoadGameBtnClick);
        SettingsBtn.onClick.AddListener(SettingsBtnClick);
        ReloadGameBtn.onClick.AddListener(ReloadGameBtnClick);
        MainMenuBtn.onClick.AddListener(MainMenuBtnClick);
        QuitBtn.onClick.AddListener(QuitBtnClick);
        BackBtn.onClick.AddListener(BackBtnClick);
    }

    private void OnEnable()
    {
        Game.Instance.InputManager.EscAction += BackBtnClick;
    }
    private void OnDisable()
    {
        Game.Instance.InputManager.EscAction -= BackBtnClick;
    }

    #region Button Listeners

    private void BackBtnClick()
    {
        GameUI.Instance.MainMenuPanel.ShowPreviousWithSound(0);
    }

    private void QuitBtnClick()
    {
        throw new NotImplementedException("Please implement 'QuitButton' functionality.");
    }

    private void MainMenuBtnClick()
    {
        GameUI.Instance.MainMenuPanel.ShowWithSound(0);
    }

    private void ReloadGameBtnClick()
    {
        throw new NotImplementedException("Please implement 'ReloadGame' functionality.");
    }

    private void SettingsBtnClick()
    {
        GameUI.Instance.SettingsPanel.ShowWithSound(0);
    }

    private void LoadGameBtnClick()
    {
        throw new NotImplementedException("Please implement 'LoadGame' functionality.");
    }

    private void SaveGameBtnClick()
    {
        throw new NotImplementedException("Please implement 'SaveGame' functionality.");
    }

    #endregion Button Listeners
}
