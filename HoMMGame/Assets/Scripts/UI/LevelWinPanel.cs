﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class LevelWinPanel : Panel
{
    [SerializeField] Button mainMenuBtn;

    private void Awake()
    {
        mainMenuBtn.onClick.AddListener(MainMenuBtnClk);
    }
    void MainMenuBtnClk()
    {
        GameUI.Instance.MainMenuPanel.Show();
    }
}
