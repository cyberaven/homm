﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameUI : MonoBehaviour
{
    public static GameUI Instance;
    public Stack<Panel> LastOpenedPanelsStack;

    [SerializeField] private IntroPanel introPanel;
    [SerializeField] private MainMenuPanel mainMenuPanel;
    [SerializeField] private SelectLevelPanel selectLevelPanel;
    [SerializeField] private NetworkLobbyPanel networkLobbyPanel;
    [SerializeField] private OptionsPanel optionsPanel;
    [SerializeField] private RatingPanel ratingPanel;
    [SerializeField] private HowToPlayPanel howToPlayPanel;
    [SerializeField] private InGameMenuPanel inGameMenuPanel;
    [SerializeField] private LevelUIPanel levelUIPanel;
    [SerializeField] private GameOverPanel gameOverPanel;
    [SerializeField] private LevelWinPanel levelWinPanel;
    [SerializeField] private SelectScenarioPanel selectScenarioPanel;
    [SerializeField] private SelectCampaignPanel selectCampaignPanel;
    [SerializeField] private AuthorsPanel authorsPanel;       
    [SerializeField] public SettingsPanel SettingsPanel;
    [SerializeField] public PauseMenu PauseMenu;
    [SerializeField] public CardBattleUI CardBattleUI;

    public IntroPanel IntroPanel { get => introPanel; set => introPanel = value; }    
    public MainMenuPanel MainMenuPanel { get => mainMenuPanel; set => mainMenuPanel = value; }
    public SelectLevelPanel SelectLevelPanel { get => selectLevelPanel; set => selectLevelPanel = value; }
    public NetworkLobbyPanel NetworkLobbyPanel { get => networkLobbyPanel; set => networkLobbyPanel = value; }
    public OptionsPanel OptionsPanel { get => optionsPanel; set => optionsPanel = value; }
    public RatingPanel RatingPanel { get => ratingPanel; set => ratingPanel = value; }
    public HowToPlayPanel HowToPlayPanel { get => howToPlayPanel; set => howToPlayPanel = value; }
    public InGameMenuPanel InGameMenuPanel { get => inGameMenuPanel; set => inGameMenuPanel = value; }
    public LevelUIPanel LevelUIPanel { get => levelUIPanel; set => levelUIPanel = value; }
    public GameOverPanel GameOverPanel { get => gameOverPanel; set => gameOverPanel = value; }
    public LevelWinPanel LevelWinPanel { get => levelWinPanel; set => levelWinPanel = value; }
    public SelectScenarioPanel SelectScenarioPanel { get => selectScenarioPanel; set => selectScenarioPanel = value; }
    public SelectCampaignPanel SelectCampaignPanel { get => selectCampaignPanel; set => selectCampaignPanel = value; }   
    public AuthorsPanel AuthorsPanel { get => authorsPanel; set => authorsPanel = value; }

    List<Panel> panels;
    private void Awake()
    {        
        Initilization();       
    }

    void Initilization()
    {
        Instance = this;
        LastOpenedPanelsStack = new Stack<Panel>();

        Canvas canvas = GetComponent<Canvas>();
        canvas.worldCamera = Camera.main;
        canvas.sortingLayerName = "MyUI";

        introPanel = Instantiate(introPanel,transform);
        selectLevelPanel = Instantiate(selectLevelPanel, transform);
        mainMenuPanel = Instantiate(mainMenuPanel, transform);
        networkLobbyPanel = Instantiate(networkLobbyPanel, transform);
        optionsPanel = Instantiate(optionsPanel, transform);
        ratingPanel = Instantiate(ratingPanel, transform);
        howToPlayPanel = Instantiate(howToPlayPanel, transform);
        inGameMenuPanel = Instantiate(inGameMenuPanel, transform);
        levelUIPanel = Instantiate(levelUIPanel, transform);
        gameOverPanel = Instantiate(gameOverPanel, transform);
        levelWinPanel = Instantiate(levelWinPanel, transform);
        selectScenarioPanel = Instantiate(selectScenarioPanel, transform);
        selectCampaignPanel = Instantiate(selectCampaignPanel, transform);        
        authorsPanel = Instantiate(authorsPanel, transform);
        SettingsPanel = Instantiate(SettingsPanel, transform);
        PauseMenu = Instantiate(PauseMenu, transform);
        CardBattleUI = Instantiate(CardBattleUI, transform);
        AddAllPanelsInList();
        HideAll();

        mainMenuPanel.Show();
    }
  
    void AddAllPanelsInList()
    {
        Panel[] p = GetComponentsInChildren<Panel>();
        panels = new List<Panel>(p);
    }
    public void HideAll()
    {        
        foreach (var panel in panels)
        {
            panel.Hide();
        }
    }

}
