﻿using UnityEngine;
using System.Collections;

public class Panel : MonoBehaviour
{
    private float CurrentTime = 0;
    private float ShowTime = 0;

    private void Update()
    {
        ShowTimer();
    }

    public void Show()
    {
        GameUI.Instance.LastOpenedPanelsStack.Push(this);
        ActivatePanel();
    }
    public void ShowWithSound(int soundId)
    {
        AudioManager.Instance.SFX.PlayOneTime(soundId);
        Show();
    }

    public void ShowPreviousScreen()
    {
        GameUI.Instance.LastOpenedPanelsStack.Pop();
        ActivatePanel();
    }
    public void ShowPreviousWithSound(int soundId)
    {
        AudioManager.Instance.SFX.PlayOneTime(soundId);
        ShowPreviousScreen();
    }

    private void ActivatePanel()
    {
        Debug.Log("Panel "+ this.gameObject.name + " show.", gameObject);
        GameUI.Instance.HideAll();
        GameUI.Instance.LastOpenedPanelsStack.Peek().gameObject.SetActive(true);
    }

    public void Show(float time)
    {
        Show();
        Debug.Log("Panel " + this.gameObject.name + " show. Time: " + time, gameObject);
        ShowTime = time;
    }  
    
    public void Hide()
    {
        Debug.Log("Panel " + this.gameObject.name + " hide.", gameObject);
        gameObject.SetActive(false);
    }

    void ShowTimer()
    {
        if(ShowTime > 0)
        {
            CurrentTime += Time.deltaTime;
            if(CurrentTime > ShowTime)
            {
                ShowTime = 0;
                CurrentTime = 0;
                Hide();
            }
        }
    }
}
