﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;


public class SelectScenarioPanel : Panel
{
    [SerializeField] Button backBtn;

    private void Start()
    {
        backBtn.onClick.AddListener(BackBtnClk);
    }

    void BackBtnClk()
    {
        GameUI.Instance.MainMenuPanel.ShowPreviousWithSound(0);
    }
}
