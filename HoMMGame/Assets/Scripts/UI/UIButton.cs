﻿using UnityEngine;
using UnityEngine.UI;
using System;
using System.Collections;

public class UIButton : MonoBehaviour
{
    [SerializeField] private KeyCode ActionKeyCode;
    [SerializeField] private int SFXAudioClipId;
    
    private void Start()
    {
        Init();
    }
    private void Init()
    {
        GetComponent<Button>().onClick.AddListener(BtnClk);  
        //Game.Instance.InputManager.SetAction(BtnClk, ActionKeyCode);
        //Game.Instance.InputManager.AddAction(BtnClk, ActionKeyCode); <-- вот так.

    }
    private void BtnClk()
    {
        Game.Instance.AudioManager.SFX.PlayOneTime(SFXAudioClipId);
        Game.Instance.GameUI.MainMenuPanel.Show();
    }
}
