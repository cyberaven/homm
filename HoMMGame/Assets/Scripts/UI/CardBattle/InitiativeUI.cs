﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class InitiativeUI : MonoBehaviour
{
    [SerializeField] private Image HealtBar, Icon, TeamColor;
    [SerializeField] private Text InitiativeText, HealthText;
    [SerializeField] public RectTransform Rect;
    [SerializeField] private Material GrayMaterial;
    public InitiativeQueueUI QueueUI;
    private Vector2? Target = null;
    private float? Appearing = null;
    private Image[] Images;
    private Text[] Textes;
    const float TopSize = 1.5f;
    private void Awake()
    {
        Images = GetComponentsInChildren<Image>();
        Textes = GetComponentsInChildren<Text>();
    }
    public void Appear(Creature creature, int i)
    {
        Icon.sprite = creature.CreatureInfo.GetIcon;
        TeamColor.color = creature.Team == 0 ? Color.green : Color.red;
        HealtBar.fillAmount = (float)creature.Health / creature.MaxHealth;
        HealthText.text = $"{creature.Health} / {creature.MaxHealth}";
        InitiativeText.text = creature.Initiative.ToString();
        Rect.anchoredPosition = Position(i);
        SetSize(i);
        Appearing = 0;
        Fade(0);
    }
    private Vector2 Position(int i) =>i == 0 ? new Vector2(-Rect.sizeDelta.x * (TopSize - 1)/2, 0)
        : new Vector2(Rect.sizeDelta.x * i + QueueUI.SplitDelta(i), 0);

    public void SetSize(int i) => transform.localScale = (i == 0) ? new Vector3(TopSize, TopSize, 1) : Vector3.one;
    public void Activate(bool active)
    {
        Icon.material = active ? null : GrayMaterial;
    }
    
    public void SetTarget(int i)
    {
        Target = Position(i);
        SetSize(i);
    }
    public void Update()
    {
        if(Target.HasValue)
        {
            if(Vector2.Distance(Target.Value, Rect.anchoredPosition) < 1)
            {
                Rect.anchoredPosition = Target.Value;
                Target = null;
            } else
            {
                Rect.anchoredPosition = Vector2.Lerp(Rect.anchoredPosition, Target.Value, 0.1f);
            }
        }
        if(Appearing.HasValue)
        {

            Fade(Appearing.Value);
            Appearing += Time.deltaTime;
            if (Appearing.Value > 1f)
            {
                Appearing = null;
            }
        }
    }
    private void Fade(float t)
    {
        foreach (var im in Images)
        {
            Color c = im.color;
            c.a = t;
            im.color = c;
        }
        foreach (var te in Textes)
        {
            Color c = te.color;
            c.a = t;
            te.color = c;
        }
    }
}
