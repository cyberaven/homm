﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine.EventSystems;
using UnityEngine;

public class CardController : MonoBehaviour, IBeginDragHandler, IDragHandler, IEndDragHandler
{

    private CardBattle BattleManager;
    private CardBattleUI UIManager;
    private Vector2 Target, PrevMousePosition;
    private bool Draged = false, OnPosition = true;
    private RectTransform Rect;
    public CardUI CardUI;
    public void Initialize()
    {
        Rect = GetComponent<RectTransform>();
        CardUI = GetComponent<CardUI>();
    }
    public void OnBeginDrag(PointerEventData eventData)
    {
        Draged = true;
        PrevMousePosition = Input.mousePosition;
        OnPosition = false;
        BattleManager.CurrentDragedCard = this;
    }

    public void OnDrag(PointerEventData eventData)
    {
        Rect.anchoredPosition += (Vector2)Input.mousePosition - PrevMousePosition;
        PrevMousePosition = Input.mousePosition;
    }

    public void OnEndDrag(PointerEventData eventData)
    {
        Draged = false;
        BattleManager.CurrentDragedCard = null;
    }
    private void Update()
    {
        if (!Draged && !OnPosition)
        {
            Rect.anchoredPosition = Vector2.Lerp(Rect.anchoredPosition, Target, 0.2f);
            if (Vector2.Distance(Rect.anchoredPosition, Target) < 0.1f)
            {
                Rect.anchoredPosition = Target;
                OnPosition = true;
            }
        }
    }
    public void SetCardToHand(RectTransform hand, int number, CardBattle cardBattle, CardBattleUI battleUI)
    {
        BattleManager = cardBattle;
        UIManager = battleUI;
        Rect.parent = hand;
        Rect.localScale = Vector3.one;
        Rect.anchoredPosition = Vector2.down * 1000;
        SetHandPosition(number);
    }
    public void SetHandPosition(int number)
    {
        Target = number * Vector2.right * (Rect.sizeDelta.x + 10);
        OnPosition = false;
        Draged = false;
    }
    public void SetToTable()
    {
        UIManager.CardDiscard(this, CardUI.Team);

    }
}
