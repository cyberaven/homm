﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class InitiativeQueueUI : MonoBehaviour
{

    [SerializeField] private InitiativeUI QueuePrefab;
    [SerializeField] private RectTransform QueueTransform, SplitImage;
    private InitiativeQueue Queue = new InitiativeQueue();
    private Stack<InitiativeUI> List = new Stack<InitiativeUI>();
    public InitiativeQueue GetClearQueue => Queue.Clearly();

    private int SplitPosition = 1;
    
    private void Start()
    {
        Queue.QueueUI = this;
        Queue.RecalculateAction += UpdateQueue;
    }
    private void OnDestroy()
    {
        Queue.RecalculateAction -= UpdateQueue;
    }
    public void SetSplitPosition(int pos)
    {
        SplitPosition = pos;
        SplitImage.anchoredPosition = new Vector2(QueuePrefab.Rect.sizeDelta.x * (pos +1), 0);
    }
    public float SplitDelta(int pos) => pos <= SplitPosition ? 0 : SplitImage.sizeDelta.x;
    public InitiativeUI GetQueueElement()
    {
        InitiativeUI bar;
        if (List.Count == 0)
        { bar = Instantiate(QueuePrefab, QueueTransform);bar.QueueUI = this; }
        else
            bar = List.Pop();
        bar.gameObject.SetActive(true);
        return bar;
    }
    public void SetQueueElement(InitiativeUI element)
    {
        element.gameObject.SetActive(false);
        List.Push(element);
    }
    void UpdateQueue()
    {
        /*int i = 0;
        foreach(Creature creature in Queue)
        {
            if (i == List.Count)
                List.Add(Instantiate(QueuePrefab, QueueTransform));
            InitiativeUI bar = List[i];
            bar.gameObject.SetActive(true);
            bar.SetCreature(creature);
            bar.SetTarget(i);
            i++;
        }
        for (; i < List.Count; i++)
            List[i].gameObject.SetActive(false);
            */
    }
}
