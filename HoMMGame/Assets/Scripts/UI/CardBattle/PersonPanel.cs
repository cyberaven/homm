﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PersonPanel : MonoBehaviour
{
    [SerializeField] private Image Icon, Skill;
    [SerializeField] private Text Level;

    public void SetPerson(BaseHero hero)
    {
        if(hero == null)
        {
            gameObject.SetActive(false);
            return;
        }
        Icon.sprite = hero.Icon;
        Skill.gameObject.SetActive(false);
        Level.text = "1";
    }
}
