﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;

public class BattleCellUI : MonoBehaviour,IPointerEnterHandler, IPointerExitHandler, IDropHandler
{
    [SerializeField] private Image Image;
    [SerializeField] public CreatureUI CreatureUI;
    private CardBattle BattleManager;
    private CardController DragedCard = null;
    public int Team;

    private void Awake()
    {

        CreatureUI = Instantiate(CreatureUI, transform);

        CreatureUI.Creature.HitAction += CreatureUI.UpdateUI;
        CreatureUI.Creature.DieAction += () =>
        {
            CreatureUI.Discard();
            BattleManager.InitiativeQueue.RemoveCreature(CreatureUI.Creature);
        };
        CreatureUI.Creature.SelectAction += () => Image.color = Color.blue;
        CreatureUI.Creature.DeselectAction += () => Image.color = Color.white;
        CreatureUI.Creature.InTop += () => CreatureUI.GetComponent<Image>().color = Color.yellow;
        CreatureUI.Creature.LeftTop += () => CreatureUI.GetComponent<Image>().color = Color.white;
    }
    public void OnPointerEnter(PointerEventData eventData)
    {
        if(BattleManager.CurrentDragedCard !=null && CanSet(BattleManager.CurrentDragedCard.CardUI))
        {
            Image.color = Color.green;
            DragedCard = BattleManager.CurrentDragedCard;
        }
    }

    public void OnPointerExit(PointerEventData eventData)
    {
        if (DragedCard != null)
        {
            Image.color = Color.white;
            DragedCard = null;
        }
    }

    public void OnDrop(PointerEventData eventData)
    {
        if (DragedCard != null)
        {
            Image.color = Color.white;
            if (DragedCard.CardUI.CurrentCard is CreatureCard creatureCard)
            {
                CreatureUI.SetCard(creatureCard, BattleManager, Team);
                BattleManager.InitiativeQueue.AddCreature(CreatureUI.Creature, BattleManager.StateMachine.CurStatement == StatementEnum.Set);
                
               
                DragedCard.SetToTable();
            }
            BattleManager.CurrentDragedCard = null;
            DragedCard = null;
        }
    }

    public void Initialization(CardBattle cardBattle,int team)
    {
        Team = team;
        BattleManager = cardBattle;
        CreatureUI.GetComponent<RectTransform>().anchoredPosition = Vector2.zero;
        CreatureUI.gameObject.SetActive(false);
    }
    public bool CanSet(CardUI card)
    {
        if (CreatureUI.IsActive && card.CurrentCard is CreatureCard)
            return false;
        return Team == card.Team;
    }
}
