﻿using System;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;

public class CreatureUI : MonoBehaviour, IPointerClickHandler,IPointerEnterHandler,IPointerExitHandler
{
   [SerializeField] private Image HealtBar, Icon;
    [SerializeField] private Text InitiativeText, AttackText, HealthText;
    public bool IsActive => gameObject.activeSelf;
    public Sprite GetIcon => Icon.sprite;
    public Creature Creature = new Creature();
    public CardBattle CardBattle;
   public void SetCard(CreatureCard creature, CardBattle cardBattle, int team)
    {
        CardBattle = cardBattle;
        Creature.SetCard(creature, team);
        gameObject.SetActive(true);
        Icon.sprite = creature.GetIcon;

        UpdateUI();

    }
    public void Discard()
    {
        gameObject.SetActive(false);
    }
    public void UpdateUI()
    {
        HealtBar.fillAmount = (float)Creature.Health / Creature.MaxHealth;
        HealthText.text = $"{Creature.Health} / {Creature.MaxHealth}";
        InitiativeText.text = Creature.Initiative.ToString();
        AttackText.text = Creature.Attack.ToString();
    }
    public void OnPointerClick(PointerEventData eventData)
    {
        CreatureUI attacker = CardBattle.CurrentSelectCreatur;
        if (attacker)
        {
            if(attacker.Creature.CanAttack(Creature))
            {
                Creature.Hit(attacker.Creature);
                CardBattle.InitiativeQueue.Scroll();
                CardBattle.CurrentSelectCreatur.Creature.DeselectAction();
                CardBattle.CurrentSelectCreatur = null;
                GetComponent<Image>().color = Color.white;
            }
        }
        else
        {
            if(CardBattle.InitiativeQueue.TopCreature == Creature && (BattleTeam?)Creature.Team == CardBattle.CurPlayer?.Team)
            {
                CardBattle.CurrentSelectCreatur = this;
                Creature.SelectAction();
            }
        }
    }

    public void OnPointerEnter(PointerEventData eventData)
    {
        CreatureUI attacker = CardBattle.CurrentSelectCreatur;
        if (attacker?.Creature.CanAttack(Creature) == true)
        {
            GetComponent<Image>().color = Color.red;
        }
    }

    public void OnPointerExit(PointerEventData eventData)
    {
        if(CardBattle.CurrentSelectCreatur?.Creature != Creature)
        GetComponent<Image>().color = Color.white;
    }
}
