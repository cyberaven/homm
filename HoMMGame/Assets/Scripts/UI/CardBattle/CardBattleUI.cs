﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class CardBattleUI : MonoBehaviour
{
    const int MaxCardInHand = 5;
    [SerializeField] private Button EndTurnButton;
    [SerializeField] private PersonPanel Hero1, Hero2, Intrigan1, Intrigan2;
    [SerializeField] private RectTransform DiscardTransform, HandCardTransform;
    [SerializeField] private BattleCellUI CellPrefab;
    [SerializeField] private RectTransform CellCorner;
    [SerializeField] private InitiativeQueueUI InitiativeQueueUI;
    public BattleCellUI[,] Cells;
   // private StateMachine[] States = new StateMachine[2];
    private CardController[,] Cards = new CardController[2, MaxCardInHand];
    private CardBattle CardBattle;
    
    public void StartBattle(Hero attacker, Hero defender)
    {
        if (Cells == null)
            Initialize();
        gameObject.SetActive(true);
        Hero1.SetPerson(attacker);
        Hero2.SetPerson(defender);
        Intrigan1.SetPerson(null);
        Intrigan2.SetPerson(null);

        CardBattle.InitiativeQueue = InitiativeQueueUI.GetClearQueue;
       
        Game.Instance.InputManager.EscAction += EndBattle;
        CardBattle.StartBattle(attacker.CardHand, defender.CardHand);
      
    }
    private void Initialize()
    {
        CardBattle = new CardBattle(this);
        int side = 2, n = 3 ;
        Cells = new BattleCellUI[side, n];
        for(int i=0;i<side;i++)
            for(int j =0;j<n;j++)
            {
                Cells[i, j] = Instantiate(CellPrefab, CellCorner);
                Cells[i, j].GetComponent<RectTransform>().anchoredPosition = new Vector2((i - 0.5f) * 200, (j - n * 0.5f) * 140);
                Cells[i, j].Initialization(CardBattle, i);
            }
    }
    public void HideCards(BattleTeam Team)
    {
        int ActiveTeam = (int)Team;
        for (int i = 0; i < MaxCardInHand; i++)
            if (Cards[ActiveTeam,i] != null)
            {
                Cards[ActiveTeam, i].gameObject.SetActive(false);
            }
    }
    public void DealCards(CardHand hand, BattleTeam Team, bool SetMode = false)
    {
        Card card;
        int ActiveTeam = (int)Team;
        for (int i = 0; i < MaxCardInHand; i++)
        {
            if (Cards[ActiveTeam, i] == null)
            {
                card = SetMode ? hand.GetRandomCreatureCard() : hand.GetRandomCard();
                if (card != null)
                {

                    Cards[ActiveTeam, i] = CardFactory.GetCard<CardController>();
                    Cards[ActiveTeam, i].Initialize();
                    Cards[ActiveTeam, i].CardUI.SetCardInformation(card, ActiveTeam);
                    Cards[ActiveTeam, i].SetCardToHand(HandCardTransform, i,CardBattle, this);
                }
            }
            else
            {
                Cards[ActiveTeam, i].gameObject.SetActive(true);
            }
        }
    }
    public void SetEndTurnEvent(UnityEngine.Events.UnityAction action)
    {
        EndTurnButton.onClick.RemoveAllListeners();
        EndTurnButton.onClick.AddListener(action);
    }
    public void EndBattle()
    {
        gameObject.SetActive(false);
        Game.Instance.InputManager.EscAction -= EndBattle;
    }
    public void CardDiscard(CardController card, int ActiveTeam)
    {
        int i;
        for (i=0;i<MaxCardInHand;i++)
        {
            if(Cards[ActiveTeam, i]== card)
            {
                CardFactory.SetCard(card.CardUI);
                Cards[ActiveTeam, i] = null;
                break;
            }
        }
        for(;i<MaxCardInHand;i++)
        {
            if (i + 1 != MaxCardInHand)
            {
                Cards[ActiveTeam, i] = Cards[ActiveTeam, i + 1];
                Cards[ActiveTeam, i]?.SetHandPosition(i);
            }
            else
            {
                 if (CardBattle.StateMachine.CurState is PlayStatement playStatement)
                 {
                    
                     Card c = playStatement.CurPlayer.Hand.GetRandomCard();
                     if (c != null)
                     {
                        Cards[ActiveTeam, i] = CardFactory.GetCard<CardController>();
                        Cards[ActiveTeam, i].Initialize();
                        Cards[ActiveTeam, i].CardUI.SetCardInformation(c, ActiveTeam);
                        Cards[ActiveTeam, i].SetCardToHand(HandCardTransform, i, CardBattle, this);
                     } else
                     {
                         Cards[ActiveTeam, i] = null;
                     }
                 } else
                 {
                Cards[ActiveTeam, i] = null;

                 }
            }
        }
    }
 
}
