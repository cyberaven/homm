﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class CardUI : MonoBehaviour
{
    [SerializeField] private Text AttackValue, InitiativeValue, HealthValue,
         CardName, CardDescription, CardType, CardFraction;
    [SerializeField] private Image CardIcon;

    [SerializeField] private RectTransform Rect, CreatureInfo;
    public int Team;
    public Card CurrentCard { get; private set; }
   
    public void SetCardInformation(Card card, int team)
    {
        CurrentCard = card;
        Team = team;
        CardName.text = card.CardName.ToString();
        CardDescription.text = card.GetCardDescription;
        CardFraction.text = card.Fraction.ToString();
        CardIcon.sprite = card.GetIcon;
        bool isCreature = false;
        if (card is CreatureCard creature)
        {
            AttackValue.text = creature.Attack.ToString();
            InitiativeValue.text = creature.Initiative.ToString();
            HealthValue.text = creature.Health.ToString();
            CardType.text = "Creature";
            isCreature = true;
        }
        else
            if (card is SpellCard spell)
        {
            CardType.text = "Spell";
        }
        CreatureInfo.gameObject.SetActive(isCreature);
    }
   
}
