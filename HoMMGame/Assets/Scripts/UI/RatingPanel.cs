﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class RatingPanel : Panel
{
    [SerializeField] private Button BackBtn;

    private void Awake()
    {
        BackBtn.onClick.AddListener(BackBtnClick);
    }
    void BackBtnClick()
    {
        GameUI.Instance.MainMenuPanel.ShowPreviousWithSound(0);
    }
}
