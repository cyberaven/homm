﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class SettingsPanel : Panel
{
    [SerializeField] public SettingsData SettingsData;

    [SerializeField] private Button BackBtn;
    [SerializeField] private Button ApplyBtn;
    [SerializeField] private Scrollbar MusicVolume;
    [SerializeField] private Image MusicFill;
    [SerializeField] private Scrollbar SoundsVolume;
    [SerializeField] private Image SoundsFill;
    [SerializeField] private Toggle MusicMute;
    [SerializeField] private Toggle SoundsMute;
    [SerializeField] private Dropdown Resolution;
    [SerializeField] private Dropdown Language;

    private void Awake()
    {
        ReadSettingsData();

    }
    private void Start()
    {
        BackBtn.onClick.AddListener(BackBtnClk);
        ApplyBtn.onClick.AddListener(ApplyBtnClk);
        MusicVolume.onValueChanged.AddListener(MusicValueChanged);
        SoundsVolume.onValueChanged.AddListener(SoundValueChanged);
        MusicMute.onValueChanged.AddListener(MusicMuteChanged);
        SoundsMute.onValueChanged.AddListener(SoundMuteChanged);
        Resolution.onValueChanged.AddListener(ResolutionChanged);
        Language.onValueChanged.AddListener(LanguageChanged);
    }
    private void OnEnable()
    {
        Game.Instance.InputManager.EscAction += BackBtnClk;
        Game.Instance.InputManager.EnterAction += ApplyBtnClk;
    }
    private void OnDisable()
    {
        Game.Instance.InputManager.EnterAction -= ApplyBtnClk;
        Game.Instance.InputManager.EscAction -= BackBtnClk;
    }
    private void ReadSettingsData()
    {
        MusicValueChanged(SettingsData.MusicVolume);
        SoundValueChanged(SettingsData.SoundVolume);
        MusicMute.isOn = !SettingsData.MusicMute;
        SoundsMute.isOn = !SettingsData.SoundMute;
        //ResolutionChanged(0); //Просто так, для хорошего настроения
        LanguageChanged((int)SettingsData.Language);
        Language.value = (int)SettingsData.Language;
    }
    private void SaveSettingsData()
    {
        SettingsData.MusicVolume = AudioManager.Instance.Music.Volume;
        SettingsData.SoundVolume = AudioManager.Instance.SFX.Volume;
        SettingsData.MusicMute = AudioManager.Instance.Music.Mute;
        SettingsData.SoundMute = AudioManager.Instance.SFX.Mute;
        SettingsData.Language = Game.Instance.Localisation.CurrentLanguage;
        //SettingsData.Resolution = ; //Просто так, для хорошего настроения
    }
    void BackBtnClk()
    {
        ReadSettingsData();//сбрасываем до того, что в файле
        GameUI.Instance.MainMenuPanel.ShowWithSound(0);
    }
    void ApplyBtnClk()
    {
        SaveSettingsData();//сохраняем в файл
        GameUI.Instance.MainMenuPanel.ShowWithSound(0);
    }
    private void MusicValueChanged(float value)
    {
        MusicFill.fillAmount = value;
        AudioManager.Instance.Music.Volume = value;
    }
    private void SoundValueChanged(float value)
    {
        SoundsFill.fillAmount = value;
        AudioManager.Instance.SFX.Volume = value;
        if (!AudioManager.Instance.SFX.IsPlaying)
            AudioManager.Instance.SFX.PlayOneTime(0);
    }
    private void MusicMuteChanged(bool value)
    {
        AudioManager.Instance.Music.Mute = !value;//value == false, когда toggle нажат => mute == true
    }
    private void SoundMuteChanged(bool value)
    {
        AudioManager.Instance.SFX.Mute = !value;
        if (!AudioManager.Instance.SFX.IsPlaying)
            AudioManager.Instance.SFX.PlayOneTime(0);
    }
    private void ResolutionChanged(int t)
    {

    }
    private void LanguageChanged(int t)
    {
        Game.Instance.Localisation?.ChangeLanguage((EnumLanguage)t);
    }

}
