﻿using UnityEngine;
using UnityEngine.UI;

public class MainMenuPanel : Panel
{
    [SerializeField] private Button CampaingBtn;
    [SerializeField] private Button ScenarioBtn;
    [SerializeField] private Button SettingsBtn;
    [SerializeField] private Button AuthorsBtn;
    [SerializeField] private Button ExitBtn;  
    [SerializeField] private Button CardBattleBtn;

    private void Awake()
    {
        CampaingBtn.onClick.AddListener(CampaingBtnClk);
        ScenarioBtn.onClick.AddListener(ScenarioBtnClk);
        SettingsBtn.onClick.AddListener(SettingsBtnClk);
        AuthorsBtn.onClick.AddListener(AuthorsBtnClk);
        ExitBtn.onClick.AddListener(ExitBtnClk);
        CardBattleBtn.onClick.AddListener(() => {  GameUI.Instance.HideAll();
            GameUI.Instance.CardBattleUI.StartBattle(Hero.GetRandomHero(), Hero.GetRandomHero());
           
        });
    }
    private void Start()
    {
        AudioManager.Instance.Music.PlayLoop(0);
    }

    void CampaingBtnClk()
    {        
        GameUI.Instance.SelectCampaignPanel.ShowWithSound(0);
        Debug.Log("CampaingBtnClk");
    }    
    void ScenarioBtnClk()
    {        
        GameUI.Instance.SelectScenarioPanel.ShowWithSound(0);
        Debug.Log("ScenarioBtnClk");
    }
    void SettingsBtnClk()
    {
        GameUI.Instance.SettingsPanel.ShowWithSound(0);
        Debug.Log("OptionsBtnClk");
    }
    void AuthorsBtnClk()
    {
        GameUI.Instance.AuthorsPanel.ShowWithSound(0);
        Debug.Log("AuthorsBtnClk");
    }
    void ExitBtnClk()
    {
        AudioManager.Instance.SFX.PlayOneTime(0);
        Debug.Log("ExitBtnClk");
    }
}
