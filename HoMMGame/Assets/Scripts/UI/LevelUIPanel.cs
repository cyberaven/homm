﻿using UnityEngine;
using UnityEngine.UI;

public class LevelUIPanel : Panel
{
    [SerializeField] private Button PauseMenuBtn;

    private void Awake()
    {
        PauseMenuBtn.onClick.AddListener(PauseMenuBtnClick);
    }
    private void OnEnable()
    {
        Game.Instance.InputManager.EscAction += PauseMenuBtnClick;
    }
    private void OnDisable()
    {
        Game.Instance.InputManager.EscAction -= PauseMenuBtnClick;
    }

    private void PauseMenuBtnClick()
    {
        GameUI.Instance.PauseMenu.ShowWithSound(0);
    }
}
