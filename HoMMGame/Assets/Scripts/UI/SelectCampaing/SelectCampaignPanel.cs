﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class SelectCampaignPanel : Panel
{
    [SerializeField] Button backBtn;
    [SerializeField] Button startLevelBtn;
    [SerializeField] LevelListScrollView levelListScrollView;
    [SerializeField] Text levelName;
    [SerializeField] Image mapPreview;
    [SerializeField] Text levelDescription;

    [SerializeField] Level selectedLevel;

    private void Start()
    {
        backBtn.onClick.AddListener(BackBtnClk);
        startLevelBtn.onClick.AddListener(StartBtnClk);

    }
    private void OnEnable()
    {
        levelListScrollView.ViewAllLevel();
        LevelButton.LevelButtonClk += LevelButtonClk;
        Game.Instance.InputManager.EnterAction += StartBtnClk;
        Game.Instance.InputManager.EscAction += BackBtnClk;
    }
    private void OnDisable()
    {
        LevelButton.LevelButtonClk -= LevelButtonClk;
        Game.Instance.InputManager.EnterAction -= StartBtnClk;
        Game.Instance.InputManager.EscAction -= BackBtnClk;
    }
    void LevelButtonClk(LevelButton levelButton)
    {
        Level l = levelButton.Level;
        levelName.text = l.LevelName;
        mapPreview.sprite = l.LevelPreviewImage;
        levelDescription.text = l.LevelDescription;
        selectedLevel = l;
    }

    void StartBtnClk()
    {
        LevelManager.Instance.StartLevel(selectedLevel);
    }
    void BackBtnClk()
    {
        GameUI.Instance.MainMenuPanel.ShowPreviousWithSound(0);
    }
}
