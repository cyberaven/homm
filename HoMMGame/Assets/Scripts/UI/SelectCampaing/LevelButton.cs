﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class LevelButton : MonoBehaviour
{
    [SerializeField] Text text;

    [SerializeField] Level level;
    public Level Level
    {
        get
        {
            return level;
        }
        set
        {
            level = value;
            text.text = level.LevelName;
        }
    }

    public delegate void LevelButtonDel(LevelButton levelButton);
    public static event LevelButtonDel LevelButtonClk;

    private void Start()
    {
        GetComponent<Button>().onClick.AddListener(ThisBtnClk);
    }
    void ThisBtnClk()
    {
        if (LevelButtonClk != null)
        {
            LevelButtonClk(this);
        }
    }   

}
