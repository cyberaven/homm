﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class LevelListScrollView : MonoBehaviour
{
    [SerializeField] GameObject contentView;
    List<Level> levels = new List<Level>();

    [SerializeField] LevelButton levelButton;

    private void OnDisable()
    {
        DestroyAllLevelBtn();
    }
    public void ViewAllLevel()
    {
        LoadAllLevel();
        foreach (Level level in levels)
        {
            LevelButton b = Instantiate(levelButton, contentView.transform);
            b.Level = level;
        }
    }
    void LoadAllLevel()
    {
        levels = LevelManager.Instance.Levels;
    }
    void DestroyAllLevelBtn()
    {
        Button[] btnList = contentView.GetComponentsInChildren<Button>();
        foreach (Button btn in btnList)
        {
            Destroy(btn.gameObject);
        }
    }
}
