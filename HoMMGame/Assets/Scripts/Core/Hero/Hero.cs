﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Hero:BaseHero
{

    public CardHand CardHand = new CardHand();
    public Hero()
    {
    }
    public static Hero GetRandomHero()
    {
        CardHand hand = new CardHand();
        for(int i = 0;i<10;i++)
        {
            hand.AddCard(Card.GetRandomCard());
        }
        Hero hero;
        if(Random.value < 0.5)
        {
            hero = new Wizard();
        } else
        {
            hero = new Warrior();
        }
        hero.CardHand = hand;
        return hero;
    }
}
