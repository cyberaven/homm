﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BaseHero 
{

    public EnumFraction Team;
    public EnumHeroes HeroType { get; protected set; }
    public Sprite Icon => SpriteHandler.GetHeroIcon();
}
