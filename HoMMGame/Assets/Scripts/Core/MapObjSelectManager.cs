﻿using UnityEngine;
using System.Collections;

public class MapObjSelectManager : MonoBehaviour
{
    private IMapObjSelectManagerLogickSystem MapObjSelectManagerLogickSystem { get; set; }

    private void Awake()
    {
        MapObjSelectManagerLogickSystem = new MapObjSelectManagerLogickSystem();
        
        MapCellObj.MapCellObjSelectEvent += MapCellObjSelected;
        MapCell.MapCellSelectEvent += MapCellSelect;
    }
    
    void MapCellSelect(IMapCell mapCell)
    {
        MapObjSelectManagerLogickSystem.MapCellSelected(mapCell);
    }
    private void MapCellObjSelected(IMapCellObj mapCellObj)
    {
        MapObjSelectManagerLogickSystem.MapCellObjSelected(mapCellObj);
    }

}
