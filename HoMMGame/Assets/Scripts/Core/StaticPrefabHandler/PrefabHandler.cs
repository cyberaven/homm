﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

static class PrefabHandler 
{
    static PrefabDataBase Data;
    static PrefabHandler()
    {
        Data = Resources.Load<PrefabDataBase>("PrefabDataBase");
    }
    public static CardUI GetCardUI => Data.CardUI;
}
