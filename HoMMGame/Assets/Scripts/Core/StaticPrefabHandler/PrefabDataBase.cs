﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "PrefabDataBase", menuName = "PrefabDataBase", order = 51)]
public class PrefabDataBase : ScriptableObject
{
    [SerializeField] public CardUI CardUI;
}
