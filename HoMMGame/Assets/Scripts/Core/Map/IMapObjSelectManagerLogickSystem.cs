﻿using UnityEngine;
using System.Collections;

public interface IMapObjSelectManagerLogickSystem
{
    void MapCellSelected(IMapCell mapCell);
    void MapCellObjSelected(IMapCellObj mapCellObj);    
}
