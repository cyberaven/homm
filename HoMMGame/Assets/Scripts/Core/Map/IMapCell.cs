﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public interface IMapCell
{
    GameObject gameObject { get; }
   
    void SelectEvent();

    IMapCellObj[] GetAllObjIn();
}
