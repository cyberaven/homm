﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class MapCellSpritesHandler : AbstractSpriteHandler
{
    [SerializeField] private Sprite DesertSprite;
    [SerializeField] private Sprite WaterSprite;
    [SerializeField] private Sprite MeadowsSprite;
    [SerializeField] private Sprite MountainsSprite;
    [SerializeField] private Sprite SwampsSprite;
    [SerializeField] private Sprite ForestSprite;    
    [SerializeField] private Sprite Arrow;
    public Sprite GetArrowSprite => Arrow;
    public Sprite GetSprite(EnumLandScape landScape)
    {
        switch(landScape)
        {
            case EnumLandScape.Desert:
                return DesertSprite;
            case EnumLandScape.Water:
                return WaterSprite;
            case EnumLandScape.Meadows:
                return MeadowsSprite;
            case EnumLandScape.Mountains:
                return MountainsSprite;
            case EnumLandScape.Swamps:
                return SwampsSprite;
            case EnumLandScape.Forest:
                return ForestSprite;            
        }

        throw new System.NotImplementedException("No Sprite for landScape type:" + landScape);
    }
}
