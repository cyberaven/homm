﻿using UnityEngine;
using System.Collections;

public class MapCellObj : MonoBehaviour, IMapCellObj
{
    public IMapCell MapCell { get; set; }

    public delegate void MapCellObjSelectDel(IMapCellObj mapCellObj);
    public static event MapCellObjSelectDel MapCellObjSelectEvent;

    private void OnMouseDown()
    {
        SelectEvent();
    }

    public void SelectEvent()
    {
        Debug.Log("MapCellObj selected: " + this, gameObject);
        if (MapCellObjSelectEvent != null)
        {
            MapCellObjSelectEvent(this);
        }
    }
    public void Move(IMapCell mapCell)
    {
        throw new System.NotImplementedException();
    }
    public void MoveTo(IMapCellObj mapCellObj)
    {
        throw new System.NotImplementedException();
    }
    public void Use(IMapCellObj mapCellObj)
    {
        throw new System.NotImplementedException();
    }
    public void OpenInfo()
    {        
        Debug.Log("MapCellObj OpenInfo: " + this, gameObject);       
    }

}
