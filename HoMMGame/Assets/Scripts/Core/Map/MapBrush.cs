﻿using UnityEngine;
using System.Collections;

public class MapBrush : MonoBehaviour
{
    [SerializeField] MapCellObj _mapCellObj;

    public MapCellObj MapCellObj { get => _mapCellObj; set => _mapCellObj = value; }

    private void Update()
    {
        FollowMapCellObjOverMouse();       
    }
    void FollowMapCellObjOverMouse()
    {
        if (_mapCellObj != null)
        {
            _mapCellObj.transform.position = Camera.main.ScreenToWorldPoint(Input.mousePosition);
            _mapCellObj.transform.position = new Vector3(_mapCellObj.transform.position.x, _mapCellObj.transform.position.y, 0);
        }
    }
}
