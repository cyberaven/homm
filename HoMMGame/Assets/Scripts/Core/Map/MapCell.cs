﻿using UnityEngine;
using System;
using System.Collections.Generic;

public class MapCell : MonoBehaviour, IMapCell, IComparable<MapCell>
{
    [SerializeField] Castle _castle;
    public Castle Castle { get => _castle; set => _castle = value; }   

    [SerializeField] Unit _unit;
    public Unit Unit { get => _unit; set => _unit = value; }
    
    [SerializeField] Chest _chest;
    public Chest Chest { get => _chest; set => _chest = value; }

    public SpriteRenderer View;

    #region Navigation
    public int Movecost;
    public MapCell[] Neighbor = new MapCell[6];
    public SpriteRenderer DebugSprite;
    public EnumLandScape LandType;
    #endregion

    public delegate void MapCellSelectDel(IMapCell mapCell);
    public static event MapCellSelectDel MapCellSelectEvent;

    #region UnityMethod
    protected void Awake()
    {
        View = GetComponent<SpriteRenderer>();
        GameObject go = new GameObject();
        go.transform.SetParent(transform);
        go.transform.localPosition = Vector3.zero;
        go.transform.localScale *= 0.2f;
        DebugSprite = go.AddComponent<SpriteRenderer>();
        DebugSprite.sprite = Game.Instance.MapCellSpritesHandler.GetArrowSprite;
        DebugSprite.sortingLayerName = "Landscape";
        DebugSprite.sortingOrder = 10;
    }
    private void OnMouseDown()
    {        
        SelectEvent();
    }
    void Start()
    {
        Setup();
    }
    #endregion

    #region Setup
    void Setup()
    {
        CreateLandscapeView();      
    }
    void CreateLandscapeView()
    {       
        View.sprite = Game.Instance.MapCellSpritesHandler.GetSprite(LandType);
    }    
    #endregion

    public void Init(int spriteId, Vector3 newPos)
    {
        SetLocalPosition(newPos);
        SetName((int)newPos.x,(int)newPos.y);        
    }
    public void Init(Vector3 newPos)
    {
        SetLocalPosition(newPos);
        SetName((int)newPos.x, (int)newPos.y); 
    }

    void SetName(int x, int y)
    {
        name = "MapCell:" + x + ":" + y;
    }
    void SetLocalPosition(Vector3 newPos)
    {
        transform.localPosition = newPos;
    }

    public void SelectEvent()
    {
        if (MapCellSelectEvent != null)
        {
            MapCellSelectEvent(this);
        }
    }

    public void PutObjIn(IMapCellObj mapCellObj)
    {
        mapCellObj.gameObject.transform.SetParent(transform);        
        mapCellObj.gameObject.transform.localPosition = new Vector3(0,0,-1); 
    }
    public IMapCellObj[] GetAllObjIn()
    {        
        IMapCellObj[] mapCellObjs = GetComponentsInChildren<IMapCellObj>();
        return mapCellObjs;
    }

    public int CompareTo(MapCell other)
    {
        return Movecost - other.Movecost;
    }
}
