﻿using UnityEngine;
using System.Collections;

public class ZoomMap : MonoBehaviour
{
    Camera cam;
    [SerializeField] private float MinZoom = 1f, MaxZoom = 15f;
    private void Awake()
    {
        cam = Camera.main;
    }

    void Update()
    {
        float d = 0;
        if(Input.GetAxis("Mouse ScrollWheel") > 0)
        {
           d = -1f;
        }
        if (Input.GetAxis("Mouse ScrollWheel") < 0)
        {
             d = 1f;
        }
        cam.orthographicSize = Mathf.Clamp(cam.orthographicSize + d, MinZoom, MaxZoom);
    }
}
