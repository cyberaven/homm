﻿using UnityEngine;
using System.Collections.Generic;

public class MapObjSelectManagerLogickSystem : IMapObjSelectManagerLogickSystem
{
    private IMapCellObj SelectedMapCellObj1;
    private IMapCellObj SelectedMapCellObj2;

    private IMapCell SelectedMapCell, PreviusMapCell;
    private Unit LastUnit;
    private List<int> CurPath;

    public void MapCellSelected(IMapCell mapCell)
    {
        PreviusMapCell = SelectedMapCell;
        SelectedMapCell = mapCell;

        CalculateReaction();
    }
    public void MapCellObjSelected(IMapCellObj mapCellObj)
    {
       
        if(SelectedMapCellObj1 == null)
        {
            SelectedMapCellObj1 = mapCellObj;
        }
        else
        {
            SelectedMapCellObj2 = mapCellObj;
        }
        if (mapCellObj is Unit)
            LastUnit = mapCellObj as Unit;
        SelectedMapCell = null;
        CalculateReaction();
    }

    private void CalculateReaction()
    {
        Unit unit = LastUnit;
        MapCell cell = SelectedMapCell as MapCell;
        MapCell prevcell = PreviusMapCell as MapCell;

        if (unit != null)
        {
            Navigation.BuildMoveCost(LastUnit.MapCell as MapCell, unit);
        }
        if(cell && unit)
        {
            if (cell.Movecost != Navigation.ImpossibleCost)
            {
                if (CurPath != null && prevcell == cell)
                {
                    unit.Move(CurPath, cell);
                    CurPath = null;
                    LastUnit = null;
                    Navigation.ClearNavigation();
                }
                else
                {
                    CurPath = Navigation.BuildPath(unit.MapCell as MapCell, cell, unit);
                }
            }
            else
            {
                LastUnit = null;
                CurPath = null;
                Navigation.ClearNavigation();
            }
        }
    }
}
