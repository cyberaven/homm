﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Map : MonoBehaviour
{
    private List<MapCell> Cells = new List<MapCell>();
    
    private void Start()
    {
        GetCells();
        FindNeighbor();
        CreateCastles();
        CreateHeroes();
        ClearNavigation();
        Navigation.CurrentMap = this;     
    }

    private void CreateCastles()
    {
        foreach (MapCell mapCell in Cells)
        {
            if(mapCell.Castle != null)
            {
                Castle castle = Instantiate(Game.Instance.CastlePrefabHandler.CastlePrefab, mapCell.transform);
                castle.MapCell = mapCell;
            }
        }
    }
    private void CreateHeroes()
    {
        foreach (MapCell mapCell in Cells)
        {
            if (mapCell.Unit != null)
            {
                Unit unit = Instantiate(mapCell.Unit, mapCell.transform);
                unit.MapCell = mapCell;
            }
        }
    }
    private void GetCells()
    {
        transform.GetComponentsInChildren<MapCell>(Cells);
        if (Cells.Count == 0)
        {
            transform.GetChild(0).transform.GetComponentsInChildren<MapCell>(Cells);
        }
        for (int i = 0; i < Cells.Count; i++)
        {
            for (int j = i + 1; j < Cells.Count; j++)
            {
                if ((Cells[i].transform.localPosition - Cells[j].transform.localPosition).sqrMagnitude < 0.001)
                {
                    Destroy(Cells[j].gameObject);
                    Cells.RemoveAt(j--);
                }
            }
        }
    }
    private void FindNeighbor()
    {
        Vector2 size = GetComponent<Grid>().cellSize;
        float dx = size.x, dy = size.y * 0.75f;
        Vector2[] d = new Vector2[6]
        {
            new Vector2(dx,0),
            new Vector2(dx*0.5f,dy),
            new Vector2(-dx*0.5f,dy),
            new Vector2(-dx,0),
            new Vector2(-dx*0.5f,-dy),
            new Vector2(dx*0.5f,-dy)
        };
        for (int i = 0; i < Cells.Count; i++)
            for (int k = 0; k < 6; k++)
                for (int j = 0; j < Cells.Count; j++)
                {
                    Vector2 to = Cells[j].transform.localPosition - Cells[i].transform.localPosition;
                    if ((to - d[k]).sqrMagnitude < 0.001)
                    {
                        Cells[i].Neighbor[k] = Cells[j];
                        break;
                    }
                }
    }

    public void ClearNavigation()
    {
        MaterialPropertyBlock block = new MaterialPropertyBlock();
        foreach (var x in Cells)
        {
            x.Movecost = Navigation.ImpossibleCost;
            x.DebugSprite.gameObject.SetActive(false);
            x.View.GetPropertyBlock(block);
            block.SetFloat("_Select", 0);
            x.View.SetPropertyBlock(block);
        }
    }
    public void DebugColor(int max)
    {

        MaterialPropertyBlock block = new MaterialPropertyBlock();
        foreach (var x in Cells)
            if (x.Movecost != Navigation.ImpossibleCost && x.Movecost != 0)

            {
                x.View.GetPropertyBlock(block);
                block.SetFloat("_T0", Time.time);
                block.SetFloat("_Select", Navigation.SelectPossible);
                x.View.SetPropertyBlock(block);
                
            }
    }
    public void DebugPath(List<int> path, MapCell from)
    {
        MaterialPropertyBlock block = new MaterialPropertyBlock();

        for(int i=0;i<path.Count;i++)
        {
            MapCell next = from.Neighbor[path[i]];

            next.DebugSprite.gameObject.SetActive(true);
            next.DebugSprite.transform.localRotation = Quaternion.Euler(0, 0, -90 + path[i] * 60);
            // x.DebugSprite.color = Color.Lerp(Color.green, Color.red, (float)x.Movecost / max);
            next.View.GetPropertyBlock(block);
            block.SetFloat("_Select", Navigation.SelectPath);
            block.SetFloat("_T0", Time.time);
            next.View.SetPropertyBlock(block);
            from = next;
        }
    }

}