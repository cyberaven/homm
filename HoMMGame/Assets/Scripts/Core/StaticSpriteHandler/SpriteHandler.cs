﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

static class SpriteHandler 
{

    static SpriteDataBase Data;
    static SpriteHandler()
    {
        Data = Resources.Load<SpriteDataBase>("SpriteDataBase");
    }
    public static Sprite GetCardIcon(EnumCard card)
    {
        return Data.CardIcons[(int)card];
    }
    public static Sprite GetHeroIcon()
    {
        return Data.HeroIcons[0];
    }
}
