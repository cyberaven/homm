﻿using System;
using System.Collections.Generic;

public class PriorityQueue<T> where T : IComparable<T>
{
    List<T> list;
    public int Count { get { return list.Count; } }

    public PriorityQueue()
    {
        list = new List<T>();
    }

    public void Enqueue(T x)
    {
        list.Add(x);
        int i = Count - 1;

        while (i > 0)
        {
            int p = (i - 1) / 2;
            if (list[p].CompareTo(x) <= 0) break;

            list[i] = list[p];
            i = p;
        }

        if (Count > 0) list[i] = x;
    }

    public T Dequeue()
    {
        T target = Peek();
        T root = list[Count - 1];
        list.RemoveAt(Count - 1);

        int i = 0;

        while (i * 2 + 1 < Count)
        {
            int a = i * 2 + 1;
            int b = i * 2 + 2;
            int c = b < Count && list[b].CompareTo(list[a]) < 0 ? b : a;

            if (list[c].CompareTo(root) >= 0) break;
            list[i] = list[c];
            i = c;
        }

        if (Count > 0) list[i] = root;
        return target;
    }

    public void Remove(T x)
    {
        int j = -1;
        for (int i = 0; i < Count; i++)
            if (list[i].CompareTo(x) == 0)
            {
                j = i; break;
            }
        if (j < 0) throw new InvalidOperationException("Queue not contains this.");
        x = list[0];
        while (j > 0)
        {
            int p = (j - 1) / 2;
            if (list[p].CompareTo(x) <= 0) break;

            list[j] = list[p];
            j = p;
        }

        if (Count > 0) list[j] = x;
        Dequeue();
    }

    public T Peek()
    {
        if (Count == 0) throw new InvalidOperationException("Queue is empty.");
        return list[0];
    }

    public void Clear()
    {
        list.Clear();
    }
}
