﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Navigation 
{
    public const int ImpossibleCost = 1 << 20;
    public const float SelectPossible = 1f, SelectPath = 2f;
    static int[] Costs = new int[(int)EnumLandScape.Size]
    {
        50,70,80,90,100,60
    };
    public static Map CurrentMap;
    public static PriorityQueue<MapCell> Queue = new PriorityQueue<MapCell>();

    static int MoveCost(MapCell from,MapCell to, Unit unit)
    {
        int movecost = Costs[(int)to.LandType];
        if (to.LandType == EnumLandScape.Mountains && to.LandType != from.LandType && unit.Fraction != EnumFraction.Voids)
            movecost += unit.MaxActionPoint / 2;
        if (unit.Fraction != EnumFraction.Atlasea && to.LandType == EnumLandScape.Water)
            movecost = ImpossibleCost;
        switch(unit.Fraction)
        {
            case EnumFraction.Rutia:if (to.LandType == EnumLandScape.Meadows) movecost -= 10;break;
            case EnumFraction.Bagudonia:if (to.LandType == EnumLandScape.Swamps) movecost -= 40;break;
            case EnumFraction.TreeWorld:if (to.LandType == EnumLandScape.Forest) movecost -= 20;break;
            case EnumFraction.Atlasea: movecost -= (to.LandType == EnumLandScape.Water) ? 15 : -5; break;
            case EnumFraction.Voids: movecost -=5;if (to.LandType == EnumLandScape.Mountains && to.LandType != from.LandType) movecost += 100; break;
            case EnumFraction.WildTribes:if (to.LandType == EnumLandScape.Desert) movecost -= 30; break;
        }
        return movecost;
    }
    public static void BuildMoveCost(MapCell from, Unit unit)
    {
        Queue.Clear();
        CurrentMap.ClearNavigation();
        from.Movecost = 0;
        Queue.Enqueue(from);
        int max = 0;
        while(Queue.Count >0)
        {
            MapCell cur = Queue.Dequeue(), next;
            max = Mathf.Max(max, cur.Movecost);
            if (cur.Movecost > unit.ActionPoint)
                continue;
            for(int i=0;i<6;i++)
                if((next = cur.Neighbor[i])!=null)
                {
                    int d = MoveCost(cur, next, unit);
                    if (d == ImpossibleCost || cur.Movecost + d >=next.Movecost)
                        continue;
                    if (next.LandType == EnumLandScape.Mountains && cur.LandType != next.LandType && cur.Movecost + d > unit.ActionPoint)
                        continue;
                    next.Movecost = cur.Movecost + d;
                    Queue.Enqueue(next);
                }
        }
        CurrentMap.DebugColor(max);
    }
    public static List<int> BuildPath(MapCell from, MapCell to, Unit unit)
    {
        List<int> path = new List<int>();
        MapCell next, cur = to;
        int debugcount = 0;
        while (cur != from && debugcount++ < 10000)
        {
            for (int i = 0; i < 6; i++)
                if ((next = cur.Neighbor[i]) != null && next.Movecost + MoveCost(next, cur, unit) == cur.Movecost)
                {
                    for (int j = 0; i < 6; j++)
                        if (next.Neighbor[j] == cur)
                        {
                            path.Add(j);
                            break;
                        }
                    cur = next;
                    break;
                }
        }

        path.Reverse();
        CurrentMap.DebugPath(path, from);
        return path;
    }
    public static void ClearNavigation() => CurrentMap.ClearNavigation();

}
