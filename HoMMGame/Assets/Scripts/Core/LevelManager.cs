﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class LevelManager : MonoBehaviour
{
    public static LevelManager Instance;

    [SerializeField] private List<Level> levels = new List<Level>();
    public List<Level> Levels { get => levels; set => levels = value; }   

    [SerializeField] private int currentLvl = 0;    

    [SerializeField] private Level currentLevel;
    public Level CurrentLevel { get => currentLevel; set => currentLevel = value; }

    private void Awake()
    {
        Initilization();        
    }
    void Initilization()
    {
        Instance = this;
    }
    
    public void StartLevel(Level level)
    {
        currentLevel = Instantiate(level);
        GameUI.Instance.LevelUIPanel.ShowWithSound(0);
    }

}
