﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public enum EnumCard 
{
    Farmer,
    Watchman,
    PatronOfLight,
    Knight,
    LightMan,
    LightServ,
    ShieldFaith,
    BlessingOfAnna,
    SunPower,
    SunStrike,
    SkeletWarrior,
    SwampZombie,
    DeathBlade,
    Necromancer,
    DeathReaper,
    SoulTrap,
    WetlandSpoilage,
    Illness,
    Epidemia,
    RaisingDead,
    Bush,
    YoungEnt,
    ForestShooter,
    WaterCultDruid,
    LivingWall,
    SpikedRoots,
    RootGrip,
    HealingNature,
    HideBehindABranch,
    SwarmOfCreeper,
    Count
}
