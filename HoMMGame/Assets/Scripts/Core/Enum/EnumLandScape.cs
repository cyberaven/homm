﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public enum EnumLandScape 
{
    Meadows,
    Forest,
    Desert,
    Swamps,
    Mountains,
    Water,
    Size
}
