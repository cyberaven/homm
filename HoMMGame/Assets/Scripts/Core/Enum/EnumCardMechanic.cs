﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public enum EnumCardMechanic 
{
    OnSet,
    Aura,
    EndTurn,
    OnDie,
    FlashAction
}
