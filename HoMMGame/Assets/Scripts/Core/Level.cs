﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public enum Complete
{
    Yes,
    No
}
public class Level : MonoBehaviour
{
    [SerializeField] Map map;

    [SerializeField] string levelName;
    public string LevelName { get => levelName; set => levelName = value; }

    [SerializeField] Complete complete = Complete.No;
    public Complete Complete { get => complete; set => complete = value; }
    
    [SerializeField] private EnumFraction fraction;
    public EnumFraction Fraction { get => fraction; set => fraction = value; }
    
    [SerializeField] Sprite levelPreviewImage;
    public Sprite LevelPreviewImage { get => levelPreviewImage; set => levelPreviewImage = value; }
    
    [SerializeField] string levelDescription;
    public string LevelDescription { get => levelDescription; set => levelDescription = value; }

    private void Start()
    {
        CreateMap();
    }
    void CreateMap()
    {
        Instantiate(map, transform);
    }
}
