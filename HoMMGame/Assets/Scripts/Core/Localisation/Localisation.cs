﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;
using System.IO;
public class Localisation : MonoBehaviour
{
    public const int LanguageCount = 2;

    private Dictionary<string, LocalisationData> Words = new Dictionary<string, LocalisationData>();
    public EnumLanguage CurrentLanguage { get; private set; }
    public Action LanguageChanged = ()=> { };//заглушка
    void Awake()
    {
        CurrentLanguage = GameUI.Instance.SettingsPanel.SettingsData.Language;
        using (StreamReader sr = new StreamReader("Assets/Localisation/Dictionary.csv"))
        {
            sr.ReadLine();//считываем хедеры
            for (int debugLine = 1; !sr.EndOfStream; debugLine++)
            {
                string[] rows = sr.ReadLine().Split(';');
                if(rows.Length <= LanguageCount)
                {
                    Debug.LogError("Не достаточно столбцов в " + debugLine + " строке");
                    continue;
                }
                Words.Add(rows[0], new LocalisationData(rows));
            }
        }
    }
    
    public void ChangeLanguage(EnumLanguage newLanguage)
    {
        CurrentLanguage = newLanguage;
        LanguageChanged();
    }

    public string GetLocalisationString(string key)
    {
        LocalisationData data;
        if (Words.TryGetValue(key, out data))
            return data.data[(int)CurrentLanguage];
        return "Noname key";
    }

}
