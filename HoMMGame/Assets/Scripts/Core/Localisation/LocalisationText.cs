﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class LocalisationText : MonoBehaviour
{
    [SerializeField] private string Key;
    private Text TextComponent;
    private void Start()
    {
        TextComponent = GetComponent<Text>();
        Game.Instance.Localisation.LanguageChanged += SetLocalisationString;
        SetLocalisationString();
    }
    private void SetLocalisationString()
    {
        TextComponent.text = Game.Instance.Localisation.GetLocalisationString(Key);
    }
}
