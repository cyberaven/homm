﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LocalisationData
{
    public string[] data = new string[Localisation.LanguageCount];

    public LocalisationData(string[] CSVrows)
    {
        for (int i = 0; i < Localisation.LanguageCount; i++)
            data[i] = CSVrows[i + 1];//+1 т.к. пропускаем ключ
    }

}
