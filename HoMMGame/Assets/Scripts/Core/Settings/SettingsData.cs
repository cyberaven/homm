﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "New SettingsData", menuName = "SettingsData", order = 51)]
public class SettingsData : ScriptableObject
{
    [SerializeField] public Vector2Int Resolution = new Vector2Int(1920, 1080);
    [Range(0,1)]
    [SerializeField] public float MusicVolume = 1;
    [Range(0, 1)]
    [SerializeField] public float SoundVolume = 1;
    [SerializeField] public bool MusicMute = false;
    [SerializeField] public bool SoundMute = false;
    [SerializeField] public EnumLanguage Language = EnumLanguage.Russian;
    [SerializeField] public bool MoveAnimation = true;
    [SerializeField] public float MoveSpeed = 4;


}
