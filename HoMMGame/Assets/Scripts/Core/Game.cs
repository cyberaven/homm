﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Game : MonoBehaviour
{
    public static Game Instance;

    [SerializeField] public GameUI GameUI;
    [SerializeField] public AudioManager AudioManager;
    [SerializeField] public LoadSaveManager loadSaveManager;
    [SerializeField] public LevelManager levelManager;
    [SerializeField] public InputManager InputManager;
    [SerializeField] public Localisation Localisation;
    [SerializeField] public MapObjSelectManager MapObjSelectManager;
    [SerializeField] public CastlePrefabHandler CastlePrefabHandler;
    [SerializeField] public UnitPrefabHandler UnitPrefabHandler;
    [SerializeField] public MapCellSpritesHandler MapCellSpritesHandler;

    private void Awake()
    {
        Instance = this;

        AudioManager = Instantiate(AudioManager, transform);
        loadSaveManager = Instantiate(loadSaveManager, transform);
        levelManager = Instantiate(levelManager, transform);

        GameUI = Instantiate(GameUI);
        InputManager = Instantiate(InputManager, transform);
        Localisation = Instantiate(Localisation, transform);
        MapObjSelectManager = Instantiate(MapObjSelectManager, transform);
        CastlePrefabHandler = Instantiate(CastlePrefabHandler, transform);
        UnitPrefabHandler = Instantiate(UnitPrefabHandler, transform);
        MapCellSpritesHandler = Instantiate(MapCellSpritesHandler, transform);
    }

    private void Start()
    {
        GameUI.MainMenuPanel.Show();
    }
    public SettingsData GetSettingsData => GameUI.SettingsPanel.SettingsData;
}
