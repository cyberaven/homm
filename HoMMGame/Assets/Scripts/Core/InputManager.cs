﻿
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class InputManager : MonoBehaviour
{
    const int EventCount = 3;

    [Header("Esc", order = 0), Header("Space", order = 1), Header("Enter", order = 2), Space(3)]
    //значения обрабатываемых клавиш и сами события
    [SerializeField] private KeyCode[] KeyCodes = new KeyCode[EventCount];
    [SerializeField] private Action[] Events = new Action[EventCount];

    public delegate void PressEvent(KeyCode code);
    public event PressEvent KeyPressed;

    //интерфейс для подписки/ отписки
    public Action EscAction { get => Events[0]; set => Events[0] = value; }
    public Action SpaceAction { get => Events[1]; set => Events[1] = value; }
    public Action EnterAction { get => Events[2]; set => Events[2] = value; }


    void Update()
    {
        if (!Input.anyKey)
        {
            return;
        }

        for (int i = 0; i < EventCount; i++)
        {
            if (Input.GetKeyDown(KeyCodes[i]))
            {
                Events[i]?.Invoke();
            }
        }
        
        if (KeyPressed != null)
        {
            for (KeyCode code = (KeyCode)0; code < KeyCode.End; code++)
            {
                KeyPressed(code);
            }
        }
    }
    public void SetAction(Action action, KeyCode code)
    {
        for (int i = 0; i < EventCount; i++)
        {
            if (action == Events[i])
            {
                KeyCodes[i] = code;
            }
        }
    }

}