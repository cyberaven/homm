﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Castle : MapCellObj, ICastle, IMapCellObj
{
    [SerializeField] private Sprite castleView;

    [SerializeField] private EnumFraction enumFraction;
    public EnumFraction EnumFraction { get => enumFraction; set => enumFraction = value; }
        
}
