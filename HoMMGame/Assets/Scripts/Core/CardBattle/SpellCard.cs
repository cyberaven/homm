﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SpellCard : Card
{
    private SpellCard(EnumCard type, EnumFraction fraction, int level)
        : base(fraction, EnumCardMechanic.FlashAction, type, level) { }

    static Dictionary<EnumCard, SpellCard> Cards = new Dictionary<EnumCard, SpellCard>();
     public static SpellCard GetCard(EnumCard type, int level)
     {
        SpellCard card = null;
        if (Cards.ContainsKey(type))
        {
            card = Cards[type];
        }
        else
        {
            switch(type)
            {
                case EnumCard.BlessingOfAnna: card = new SpellCard(type, EnumFraction.Rutia, 1);break;
                case EnumCard.LightServ: card = new SpellCard(type, EnumFraction.Rutia, 1);break;
                case EnumCard.ShieldFaith: card = new SpellCard(type, EnumFraction.Rutia, 1);break;
                case EnumCard.SunPower: card = new SpellCard(type, EnumFraction.Rutia, 1);break;
                case EnumCard.SunStrike: card = new SpellCard(type, EnumFraction.Rutia, 1);break;
                case EnumCard.SoulTrap: card = new SpellCard(type, EnumFraction.Bagudonia, 1); break;
                case EnumCard.WetlandSpoilage: card = new SpellCard(type, EnumFraction.Bagudonia, 1); break;
                case EnumCard.Illness: card = new SpellCard(type, EnumFraction.Bagudonia, 1); break;
                case EnumCard.Epidemia: card = new SpellCard(type, EnumFraction.Bagudonia, 1); break;
                case EnumCard.RaisingDead: card = new SpellCard(type, EnumFraction.Bagudonia, 1); break;
                case EnumCard.SpikedRoots: card = new SpellCard(type, EnumFraction.TreeWorld, 1); break;
                case EnumCard.RootGrip: card = new SpellCard(type, EnumFraction.TreeWorld, 1); break;
                case EnumCard.HealingNature: card = new SpellCard(type, EnumFraction.TreeWorld, 1); break;
                case EnumCard.HideBehindABranch: card = new SpellCard(type, EnumFraction.TreeWorld, 1); break;
                case EnumCard.SwarmOfCreeper: card = new SpellCard(type, EnumFraction.TreeWorld, 1); break;
            }
            if (card != null)
                Cards.Add(type, card);
        }
        return card;
     }
}
