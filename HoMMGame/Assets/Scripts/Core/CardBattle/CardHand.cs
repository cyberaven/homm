﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CardHand : MonoBehaviour,IEnumerable
{
   public  class Pair<U, V>
    {
        public U first;
        public V second;
        public Pair(U u, V v)
        {
            first = u;
            second = v;
        }
    }
    private List<Pair<int, Card>> Cards = new List<Pair<int, Card>>();
    public int CardTypeCount => Cards.Count;
    public int TotalCount()
    {
        int s = 0;
        for (int i = 0; i < Cards.Count; i++)
            s += Cards[i].first;
        return s;
    }
    private Card GetRandomSeparateCard(Func<Card, bool> separator)
    {
        int max = 0;
        for (int i = 0; i < Cards.Count; i++)
            if (separator(Cards[i].second))
                max += Cards[i].first;
        int k = UnityEngine.Random.Range(0, max);
        for (int i = 0; i < Cards.Count; i++)
            if (separator(Cards[i].second))
            {
                if (k < Cards[i].first)
                {
                    Card card = Cards[i].second;
                    Cards[i].first--;
                    if (Cards[i].first == 0)
                        Cards.RemoveAt(i);
                    return card;
                }
                else
                {
                    k -= Cards[i].first;
                }
            }
        return null;
    }
    public Card GetRandomCard() => GetRandomSeparateCard((x) => true);
    public CreatureCard GetRandomCreatureCard() => (CreatureCard)GetRandomSeparateCard((x) => x is CreatureCard);
    public void AddCard(Card card)
    {
        AddCard(card, 1);
    }
    public void AddCard(Card card, int count)
    {
        int i = Cards.FindIndex((x) => x.second == card);
        List<Vector2> l=new List<Vector2>();
        var p = new Pair<int, int>(0, 1);
        p.first += 1;
        if (i < 0)
            Cards.Add(new Pair<int, Card>(count, card));
        else
            Cards[i].first += count;
    }
    public int Count(Card card)
    {
        int i = Cards.FindIndex((x) => x.second == card);
        return i < 0 ? 0 : Cards[i].first;
    }
    public int Count(int i)
    {
        return Cards[i].first;
    }

    public IEnumerator GetEnumerator()
    {
        return ((IEnumerable)Cards).GetEnumerator();
    }
}
