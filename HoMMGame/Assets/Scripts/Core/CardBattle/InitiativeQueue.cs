﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class InitiativeQueue :IEnumerable
{
    private List<(Creature,InitiativeUI)> Queue = new List<(Creature, InitiativeUI)>();
    public event Action RecalculateAction;
    public event Action<int> AddAction, RemoveAction, ScrollAction;
    private int Top;
    public InitiativeQueueUI QueueUI;
    public int Count => Queue.Count;
    public InitiativeQueue Clearly()
    {
        Queue.Clear();
        Top = -1;
        return this;
    }
    public void ResetTop()
    {
        Top = 0;
        RecalculateAction();
    }
    private void SetTargets()
    {

        QueueUI.SetSplitPosition(Queue.Count - Top - 1);
        for (int i = 0; i < Queue.Count; i++)
            Queue[i].Item2.SetTarget((i - Top + Queue.Count) % Queue.Count);
    }
    public void AddCreature(Creature creature, bool resetTop = false)
    {
        var top = TopCreature;
        int i;
        for (i = 0; i < Queue.Count; i++)
            if (creature.CompareTo(Queue[i].Item1) <= 0)
                break;
        var p = (creature, QueueUI.GetQueueElement());
        Queue.Insert(i, p);
        if (top != TopCreature || top == null)
            Top++;
        if (resetTop)
            Top = 0;
        p.Item2.Activate(resetTop||false);
        Queue[i].Item2.Appear(creature, (i + Top) % Queue.Count);
        SetTargets();
    }
    public void RemoveCreature(Creature creature)
    {
        var top = TopCreature;
        int i = Queue.FindIndex((x) => x.Item1 == creature);
        QueueUI.SetQueueElement(Queue[i].Item2);
        Queue.RemoveAt(i);
        if (top != TopCreature && top != creature)
            Top--;
        SetTargets();


    }
    public Creature TopCreature => Top < 0 ? null : Queue[Top].Item1;
    public InitiativeUI TopUI => Top < 0 ? null : Queue[Top].Item2;

    public void Scroll()
    {
        TopCreature?.LeftTop();
        Top = (Top + 1) % Queue.Count;
        var top = TopCreature;
        if (top != null) {
            top.InTop();
            if (!top.IsActive)
            {
                top.Activate();
                TopUI.Activate(true);
                Scroll();
                return;
            }
        }
        SetTargets();
    }

    public IEnumerator GetEnumerator()
    {
        return new InitiativeQueueEnumarator(this);
    }
    class InitiativeQueueEnumarator : IEnumerator
    {
        public InitiativeQueue queue;
        public InitiativeQueueEnumarator(InitiativeQueue initiative) => (queue) = (initiative);
        public object Current => queue.Queue[ (pos + queue.Top) % queue.Count];
        int pos =- 1;

        public bool MoveNext()
        {
            pos++;
            return pos < queue.Count;
        }

        public void Reset()
        {
            pos = -1;
        }
    }
}


