﻿using System;
using System.Collections.Generic;
using UnityEngine;

public class Creature :IComparable<Creature>
{

    public int Health, MaxHealth, Initiative, Attack, Team;
    public bool IsActive { get; private set; }
    public CreatureCard CreatureInfo;
    public Action HitAction, DieAction, SelectAction, DeselectAction, InTop, LeftTop;
    public void SetCard(CreatureCard card, int team) =>
    (CreatureInfo, Team, Health, MaxHealth, Initiative, Attack, IsActive) = (card, team, card.Health, card.Health, card.Initiative, card.Attack, false);

    public bool CanAttack(Creature other) => other.Team != Team;
    public int CompareTo(Creature other)
    {
        if (other.Initiative != Initiative)
            return other.Initiative - Initiative;
        if (other.MaxHealth != MaxHealth)
            return MaxHealth - other.MaxHealth;

        return Team - other.Team;
    }
    public void Activate() => IsActive = true;
    public void Hit(Creature enemy)
    {
        Health -= enemy.Attack;
        
        if(Health <= 0)
        {
            Health = 0;
            DieAction?.Invoke();
        } else
        {
            HitAction?.Invoke();
        }

    }
}
