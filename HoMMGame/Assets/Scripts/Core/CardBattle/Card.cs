﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Card
{
    public readonly EnumFraction Fraction;
    public readonly EnumCardMechanic CardMechanic;
    public readonly EnumCard CardName;
    public readonly int Level;
    public string GetCardType => this is SpellCard ? "Spell" : "Creature";

    public Sprite GetIcon => SpriteHandler.GetCardIcon(CardName);
    protected Card(EnumFraction fraction, EnumCardMechanic mechanic, EnumCard cardName, int Level) =>
        (Fraction, CardMechanic, CardName, this.Level) = (fraction, mechanic, cardName, Level);
    
    public static Card GetRandomCard()
    {
        if (Random.value > 0.5f)
        {
            return CreatureCard.GetCard((EnumCard)Random.Range(0, 5), 1);

        } else
        {
            return SpellCard.GetCard((EnumCard)Random.Range(5, 10), 1);
        }
    }
    public string GetCardDescription { get {
            string s = "Not now";
            switch(CardName)
            {
                case EnumCard.Farmer:s = "Клич: Вспахивает выбранную землю превращая её в луг на любой стороне"; break;
                case EnumCard.Watchman:s = "Клич: Получает +1 к здоровью и атаке за существо с каждой стороны. Если стоит на лугу: получает божественный щит"; break;
                case EnumCard.PatronOfLight: s = "Клич: Увеличивает запас здоровья выбранного существа на 3. Если стоит на лугу: В конце каждого хода даёт +1 к атаке случайному союзнику"; break;
                case EnumCard.Knight:s = "Аура: все ваши существа получают +1/+1. Если стоит на лугу: невосприимчив к магии"; break;
                case EnumCard.LightMan:s = "Клич: земли по окраинам этого существа превращаются в луг. Если стоит на лугу: ослепляет выбранное сущ-во врага на 1 ход"; break;
                case EnumCard.LightServ:s = "Приравнивает показатель здоровья и атаки существа к 1/1"; break;
                case EnumCard.ShieldFaith:s = "Дает существу +2 к атаке и божественный щит"; break;
                case EnumCard.BlessingOfAnna:s = "Существо игрока до конца хода существо становится неуязвимым и получает +1/+1"; break;
                case EnumCard.SunPower:s = "Наносит 2 единицы урона за каждый луг в бою"; break;
                case EnumCard.SunStrike:s = "Наносит 2 единицы урона всем существам противника. Если есть 4 луга на стороне игрока: 3 единицы урона"; break;
                case EnumCard.SkeletWarrior:s = "После смерти наносит урон равный своей атаке существу напротив"; break;
                case EnumCard.SwampZombie:s = "Превращает землю под собой и у противника напротив в болото"; break;
                case EnumCard.DeathBlade:s = "Наносит 1 единицу урона всем противникам"; break;
                case EnumCard.Necromancer:s = "Клич если на болоте: поднимает по скелету на каждом болоте. Хрип если на болоте: Призывает после себя болотного зомби"; break;
                case EnumCard.DeathReaper:s = "На болоте: получает +1/+1 за смерть каждого существа. Хрип на болоте: атака всех врагов уменьшается на 1"; break;
                case EnumCard.SoulTrap:s = "Следующее существо, которое умрёт воскреснет в выбранном болоте на вашей стороне"; break;
                case EnumCard.WetlandSpoilage:s = "Превращает 3 любых земли на любой стороне в болото"; break;
                case EnumCard.Illness:s = "Можно использовать если есть не менее 2х болот. Выбранное существо умрёт в конце хода. Ваши случайные 2 болота превращаются в луг"; break;
                case EnumCard.Epidemia:s = "Можно использовать если есть не менее 3х болот. Срабатывают все хрипы ваших существ. Ваши случайные 3 болота превращаются в луг"; break;
                case EnumCard.RaisingDead: s = "Призывает скелетов-воинов на все болота вашей стороны"; break;
                case EnumCard.Bush: s = "Хрип: После своей смерти превращает свою землю в лес. Если стоит на лесу: в конце каждого хода получает +2/+2"; break;
                case EnumCard.YoungEnt: s = "Клич: Оплетает корнями выбранное существо. Оно пропускает ход. Если стоит на лесу: в конце каждого хода получает + 2 / +2"; break;
                case EnumCard.ForestShooter: s = "Клич если стоит в лесу: наносит 3 единицы урона существу противника"; break;
                case EnumCard.WaterCultDruid: s = "Клич: Превращает выбранную землю в лес. Если стоит на лесу: в конце каждого хода дает кустику, энту или стене +1/+1 "; break;
                case EnumCard.LivingWall: s = "Провокация. Если стоит на лесу: в конце каждого хода получает + 2 / +2"; break;
                case EnumCard.SpikedRoots: s = "Наносит 2 единицы урона существу. Оно пропускает свой следующий ход"; break;
                case EnumCard.RootGrip: s = "Требуется три леса. Все существа противника пропускают ход. Ваши три леса превращаются в горы"; break;
                case EnumCard.HealingNature: s = "Восстанавливает всем вашим существам 2 единицы здоровья"; break;
                case EnumCard.HideBehindABranch: s = "Укрывает выбранное существо до конца хода. Оно не может атаковать и его не смогут атаковать пока действует эффект"; break;
                case EnumCard.SwarmOfCreeper: s = "Призовите 4 корня, которые умрут в конце этого хода. Они могут атаковать сразу без очереди"; break;
            }
            return s;
        } }

}
