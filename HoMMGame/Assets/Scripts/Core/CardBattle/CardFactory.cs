﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

static class CardFactory 
{
    static Stack<CardUI> Pool = new Stack<CardUI>();
    
    public static T GetCard<T>() where T : Component
    {
        CardUI card = GetCard();
        T type;
        if(!card.TryGetComponent<T>(out type))
        {
            type = card.gameObject.AddComponent<T>();
        }
        return type;
    }
    public static CardUI GetCard()
    {
        CardUI card;
        if (Pool.Count > 0)
            card = Pool.Pop();
        else
            card = GameObject.Instantiate(PrefabHandler.GetCardUI);
        card.gameObject.SetActive(true);
        return card;
    }
    public static void SetCard(CardUI card)
    {
        card.gameObject.SetActive(false);
        Pool.Push(card);
    }
}
