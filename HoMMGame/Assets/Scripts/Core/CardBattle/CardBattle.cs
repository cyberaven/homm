﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CardBattle 
{
    public Player Player1, Player2;
    private CardBattleUI UIManager;
    public StateMachine StateMachine;
    public CardController CurrentDragedCard;
    public CreatureUI CurrentSelectCreatur;
    public InitiativeQueue InitiativeQueue;
    public Player CurPlayer => (StateMachine.CurState is PlayStatement play) ? play.CurPlayer : null;
    public CardBattle(CardBattleUI cardBattleUI) =>
        (UIManager, StateMachine) = (cardBattleUI, new StateMachine(this, cardBattleUI));
    
    public void StartBattle(CardHand attacker, CardHand deffender)
    {
        Player1 = new Player(attacker, BattleTeam.Attacker);
        Player2 = new Player(deffender, BattleTeam.Deffender);
        StateMachine.StartStatement(StatementEnum.Set);
    }
    public void ActivateCards()
    {
        foreach (var controller in UIManager.Cells)
            controller.CreatureUI.Creature.Activate();
    }
}
