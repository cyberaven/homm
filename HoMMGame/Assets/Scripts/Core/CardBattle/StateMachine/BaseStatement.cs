﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BaseStatement
{
    protected CardBattleUI UIManager;
    protected CardBattle BattleManager;
    protected StateMachine StateMachine;
    public BaseStatement(CardBattle cardBattle, CardBattleUI cardBattleUI, StateMachine stateMachine) =>
    (BattleManager, UIManager, StateMachine) = (cardBattle, cardBattleUI, stateMachine);
    
    public virtual void StartStatement()
    {
    }
    public virtual void StopStatement()
    {

    }

}
