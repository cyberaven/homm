﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class StateMachine 
{
    public BaseStatement CurState { get; private set; }
    private CardBattleUI UIManager;
    private CardBattle BattleManager;
    public StatementEnum CurStatement { get; private set; }
    public StateMachine(CardBattle cardBattle, CardBattleUI cardBattleUI) =>
        (BattleManager, UIManager) = (cardBattle, cardBattleUI);
    public void StartStatement(StatementEnum statement)
    {
        if(CurState != null)
        {
            CurState.StopStatement();
        }
        CurState = GetStatement(statement);
        CurState.StartStatement();
    }

    BaseStatement GetStatement(StatementEnum statement)
    {
        CurStatement = statement;
        switch (statement)
        {
            case StatementEnum.Play:return new PlayStatement(BattleManager, UIManager, this);
            case StatementEnum.Set: return new SetStatement(BattleManager, UIManager, this);
        }
        return null;
    }
}
