﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayStatement : BaseStatement
{
    private Player Attacker, Deffender;
    private int Turn;
    public int GetTurn => Turn / 2 + 1;
    public Player CurPlayer => Turn % 2 == 0 ? Attacker : Deffender;
    public PlayStatement(CardBattle cardBattle, CardBattleUI cardBattleUI, StateMachine stateMachine) :base (cardBattle, cardBattleUI, stateMachine)=>
   (Attacker, Deffender) = (cardBattle.Player1, cardBattle.Player2);
    public override void StartStatement()
    {
        UIManager.HideCards(Attacker.OtherTeam);
        UIManager.DealCards(Attacker.Hand, Attacker.Team);

        Turn = 0;
        UIManager.SetEndTurnEvent(EndTurnAction);

        BattleManager.ActivateCards();
        BattleManager.InitiativeQueue.TopCreature.InTop();
    }
    private void EndTurnAction()
    {
        Turn++;
        Player player = CurPlayer;
        UIManager.HideCards(player.OtherTeam);
        UIManager.DealCards(player.Hand, player.Team);
    }
}
