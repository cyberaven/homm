﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SetStatement : BaseStatement
{
    private Player Attacker, Deffender;
    public SetStatement(CardBattle cardBattle, CardBattleUI cardBattleUI, StateMachine stateMachine) : base(cardBattle, cardBattleUI, stateMachine) =>
        (Attacker, Deffender) = (cardBattle.Player1, cardBattle.Player2);
    
    public override void StartStatement()
    {
        if (Deffender.AI)
        {

        }
        else
        {
            UIManager.HideCards(BattleTeam.Attacker);
            UIManager.DealCards(Deffender.Hand, BattleTeam.Deffender, true);
        }
        UIManager.SetEndTurnEvent(AttackerSet);
    }
    private void AttackerSet()
    {
        if (Attacker.AI)
        {

        }
        else
        {
            UIManager.HideCards(BattleTeam.Deffender);
            UIManager.DealCards(Attacker.Hand, BattleTeam.Attacker, true);
        }

        UIManager.SetEndTurnEvent(() => StateMachine.StartStatement(StatementEnum.Play));
    }
}
