﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public  class CreatureCard : Card
{
    public readonly int Attack;
    public readonly int Initiative;
    public readonly int Health;

    private CreatureCard(EnumCard type, EnumCardMechanic mechanic, EnumFraction fraction, int attack, int health, int initiative, int level) :
        base(fraction, mechanic, type, level) =>
        (Attack, Initiative, Health) = (attack, initiative, health);
    
    static Dictionary<EnumCard, CreatureCard> Cards = new Dictionary<EnumCard, CreatureCard>();
    public static CreatureCard GetCard(EnumCard type,int level)
    {
        CreatureCard card = null;
        if (Cards.ContainsKey(type))
        {
            card = Cards[type];
        }
        else
        {
            switch (type)
            {
                case EnumCard.Farmer: card = new CreatureCard(type, EnumCardMechanic.OnSet, EnumFraction.Rutia, 1, 2, 8, level); break;
                case EnumCard.Watchman: card = new CreatureCard(type, EnumCardMechanic.OnSet, EnumFraction.Rutia, 2, 4, 9, level);break;
                case EnumCard.PatronOfLight: card = new CreatureCard(type, EnumCardMechanic.OnSet, EnumFraction.Rutia, 3, 3, 7, level);break;
                case EnumCard.Knight: card = new CreatureCard(type, EnumCardMechanic.Aura, EnumFraction.Rutia, 4, 6, 15, level);break;
                case EnumCard.LightMan: card = new CreatureCard(type, EnumCardMechanic.OnSet, EnumFraction.Rutia, 4, 7, 10, level);break;

                case EnumCard.SkeletWarrior: card = new CreatureCard(type, EnumCardMechanic.OnSet, EnumFraction.Bagudonia, 2, 2, 10, level); break;
                case EnumCard.SwampZombie: card = new CreatureCard(type, EnumCardMechanic.OnSet, EnumFraction.Bagudonia, 2, 3, 6, level); break;
                case EnumCard.DeathBlade: card = new CreatureCard(type, EnumCardMechanic.OnSet, EnumFraction.Bagudonia, 3, 3, 11, level); break;
                case EnumCard.Necromancer: card = new CreatureCard(type, EnumCardMechanic.Aura, EnumFraction.Bagudonia, 3, 5, 12, level); break;
                case EnumCard.DeathReaper: card = new CreatureCard(type, EnumCardMechanic.OnSet, EnumFraction.Bagudonia, 4, 4, 13, level); break;

                case EnumCard.Bush: card = new CreatureCard(type, EnumCardMechanic.OnSet, EnumFraction.TreeWorld, 1, 1, 7, level); break;
                case EnumCard.YoungEnt: card = new CreatureCard(type, EnumCardMechanic.OnSet, EnumFraction.TreeWorld, 3, 3, 6, level); break;
                case EnumCard.ForestShooter: card = new CreatureCard(type, EnumCardMechanic.OnSet, EnumFraction.TreeWorld, 1, 1, 15, level); break;
                case EnumCard.WaterCultDruid: card = new CreatureCard(type, EnumCardMechanic.Aura, EnumFraction.TreeWorld, 2, 3, 16, level); break;
                case EnumCard.LivingWall: card = new CreatureCard(type, EnumCardMechanic.OnSet, EnumFraction.TreeWorld, 4, 4, 5, level); break;
            }
            if (card != null)
                Cards.Add(type, card);
        }
        return card;

    }
}
