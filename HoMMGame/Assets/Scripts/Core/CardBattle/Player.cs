﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Player 
{
    public CardHand Hand { get; private set; }
    public BattleTeam Team { get; private set; }
    public bool AI { get; private set; }
    public BattleTeam OtherTeam => Team == BattleTeam.Attacker ? BattleTeam.Deffender : BattleTeam.Attacker;
    public Player(CardHand cardHand, BattleTeam battleTeam, bool isAI = false) =>
         (Hand, Team, AI) = ( cardHand, battleTeam, isAI);
   

}
