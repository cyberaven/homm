﻿using UnityEngine;
using System.Collections;

public class Mountains : MapCell
{
    private void Awake()
    {
        LandType = EnumLandScape.Mountains;
        base.Awake();
    }
}
