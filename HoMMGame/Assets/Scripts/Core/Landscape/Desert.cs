﻿using UnityEngine;
using System.Collections;

public class Desert : MapCell
{
    private void Awake()
    {
        LandType = EnumLandScape.Desert;
        base.Awake();
    }
}
