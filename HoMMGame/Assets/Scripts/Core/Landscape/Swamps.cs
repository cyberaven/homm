﻿using UnityEngine;
using System.Collections;

public class Swamps : MapCell
{
    private void Awake()
    {
        LandType = EnumLandScape.Swamps;
        base.Awake();
    }
}
