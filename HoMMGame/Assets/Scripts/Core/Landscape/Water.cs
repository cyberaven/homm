﻿using UnityEngine;
using System.Collections;

public class Water : MapCell
{
    private void Awake()
    {
        LandType = EnumLandScape.Water;
        base.Awake();
    }
}
