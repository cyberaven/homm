﻿using UnityEngine;
using System.Collections;

public class Meadows : MapCell
{
    private void Awake()
    {
        LandType = EnumLandScape.Meadows;
        base.Awake();
    }
}
