﻿using UnityEngine;
using System.Collections;

public class Forest : MapCell
{
    private void Awake()
    {
        LandType = EnumLandScape.Forest;
        base.Awake();
    }

}
