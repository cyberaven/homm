﻿using UnityEngine;
using System.Collections;

public interface IUnit
{
    EnumSide Side { get; set; }   
}
