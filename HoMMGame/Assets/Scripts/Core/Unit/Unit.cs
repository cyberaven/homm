﻿using UnityEngine;
using System.Collections.Generic;
using System.Collections;

public class Unit : MapCellObj, IUnit
{   
    [SerializeField] private EnumSide _side;
    public EnumSide Side { get => _side; set => _side = value; }

    private UnitSpriteHandler UnitSpriteHandler { get; set; }

    public int MaxActionPoint = 1000, ActionPoint = 1000;
    public EnumFraction Fraction;
    public Hero Hero { get; private set; }
    private void Awake()
    {
       // UnitSpriteHandler = GetComponent<UnitSpriteHandler>();

    }
    private void Start()
    {
       // GetComponent<SpriteRenderer>().sprite = UnitSpriteHandler.GetRandomSprite();
    }
    public void Move(List<int>path, MapCell to)
    {
        if (Game.Instance.GetSettingsData.MoveAnimation)
        {
            StartCoroutine(MoveCoroutine(path, MapCell as MapCell));
        } else
        {
            transform.SetParent(to.transform);
            transform.localPosition = Vector3.zero;
        }
        (MapCell as MapCell).Unit = null;
        to.Unit = this;
        this.MapCell = to;
    }
    IEnumerator MoveCoroutine(List<int> path, MapCell from)
    {
        if (path.Count == 0)
            yield return null;
        int i = 0;
        MapCell cur = from;
        MapCell next = cur.Neighbor[path[i]];
        float t = 0;
        while (true)
        {
            transform.position = Vector3.LerpUnclamped(cur.transform.position, next.transform.position, t);
            yield return new WaitForEndOfFrame();
            t += Time.deltaTime * Game.Instance.GetSettingsData.MoveSpeed;
            if(t >=1)
            {
                if(++i < path.Count)
                {

                    cur = next;
                    next = cur.Neighbor[path[i]];
                    t = 0;
                } else
                {
                    break;
                }
            }

        }
        transform.SetParent(next.transform);
        transform.localPosition = Vector3.zero;
    }

}



