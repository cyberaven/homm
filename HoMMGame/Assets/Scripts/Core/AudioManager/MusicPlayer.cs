﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MusicPlayer : AudioPlayer
{

    public void PlayLoop(int id)
    {
        Source.clip = audioClips[id];
        Source.loop = true;
        Source.Play(0);
    }
}
