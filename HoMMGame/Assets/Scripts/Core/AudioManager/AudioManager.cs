﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class AudioManager : MonoBehaviour
{
    public static AudioManager Instance;

    [SerializeField] public MusicPlayer Music;//без Instansiate поскольку они в префабе и так есть
    [SerializeField] public EffectsPlayer SFX;
    private void Awake()
    {
        Initilization();        
    }

    void Initilization()
    {
        Instance = this;         
    }
}
