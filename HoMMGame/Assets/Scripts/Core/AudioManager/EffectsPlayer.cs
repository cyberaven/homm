﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EffectsPlayer : AudioPlayer
{
    public void PlayOneTime(int id)
    {
        Source.PlayOneShot(audioClips[id]);
    }
    public bool IsPlaying => Source.isPlaying;
}
