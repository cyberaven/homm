﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AudioPlayer : MonoBehaviour
{
    [SerializeField] protected List<AudioClip> audioClips;
    [SerializeField] protected AudioSource Source;
    public float Volume
    {
        get => Source.volume;
        set { Source.volume = value;}
    }
    public bool Mute
    {
        get => Source.mute;
        set { Source.mute = value;}
    }
    public void Stop()
    {
        Source.Stop();
    }
}
