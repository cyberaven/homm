﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public abstract class AbstractSpriteHandler : MonoBehaviour, ISpritesHandler
{
    [SerializeField] List<Sprite> _sprites;
    public List<Sprite> Sprites { get => _sprites; set => _sprites = value; }

    public Sprite GetRandomSprite()
    {
        Sprite sprite = Sprites[Random.Range(0, Sprites.Count)];
        return sprite;
    }
    public Sprite GetSpriteFromId(int id)
    {
        if (Sprites[id] != null)
        {
            Sprite sprite = Sprites[id];
            return sprite;
        }
        else
        {
            Debug.Log("No Sprite with ID:" + id);
            return null;
        }
    }
}
